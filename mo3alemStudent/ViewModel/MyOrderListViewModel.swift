//
//  MyOrderListViewModel.swift
//  mo3alemStudent
//
//  Created by hassan on 9/8/18.
//  Copyright © 2018 hassan. All rights reserved.
//

import Foundation

class MyOrderListViewModel {
    
    var SuccessResponse : ((_ data:[[OrderNewModel]]) -> Void)!
    var FailurResponse : ((_ message : String) -> Void)!
    
    func GetAllMyOrders() -> Void {
        let parameter = ["apiToken":UserDataActions.getApiToken()] as [String : AnyObject]
        RestAPIRequest.SendRequest(mod: BaseModel<OrderListModel>.self, urlPath: RoutesFactory.URLwithTokenParameter(route: .listOrders).link, method: .post, parameter: parameter) { (response) in
            switch response {
                
            case .success(let model):
                if model.status == 200 {
                    let arr = [(model.data?.today)! , (model.data?.before)!]
                    self.SuccessResponse!(arr)
                }else{
                    self.FailurResponse!((model.error?.joined(separator: ","))!)
                }
            case .failure(let error):
                self.FailurResponse!((error?.localizedDescription)!)
            }
        }
    }
    
    
    
}

