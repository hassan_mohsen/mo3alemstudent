//
//  LoginViewModel.swift
//  Mo3alemTeacher
//
//  Created by hassan on 8/29/18.
//  Copyright © 2018 hassan. All rights reserved.
//

import Foundation

typealias OperationClouser = (() -> Void)
typealias OperationClouserWithStrParameter = ((_ message: String) -> Void)


class ClousersBase {
    var successEvent : OperationClouser?
    var failureEvent : OperationClouserWithStrParameter?
    
}



class LoginViewModel : ClousersBase {
    
    
    private var e_mail : String!
    private var password : String!
    
    var notActiveEvent : OperationClouserWithStrParameter?
    
    
    
    func setModelValues(email : String , pass : String) -> Void {
        self.e_mail = email
        self.password = pass
    }
    
    func makeMeLogin() -> Void {
        let parameter = ["email" : e_mail! ,
                         "password" : password! ,
                         "type" : "0"] as [String : AnyObject]
        
        RestAPIRequest.SendRequest(mod: BaseModel<UserLogin>.self, urlPath: RoutesFactory.URLClear(route: .login).link, method: .post, parameter: parameter) { (response) in
                print("in view model")
            switch response {
                
            case .success(let model):
                if model.status == 200 {
                    // minus cash Data
                    UserDataActions.cashUserModel(user: model.data!)
                    self.successEvent!()
                }else if model.status == 406{ //not active
                    self.notActiveEvent!(model.error?.joined(separator: ",") ?? "الحساب غير مفعل من فضلك قم بالتفعيل الآن")
                }else{
                    print("errors >>>> \(model.error)")
                    self.failureEvent!(model.error!.joined(separator: ","))
                }
                
            case .failure(let error):
                print("in failuer")
                self.failureEvent!((error?.localizedDescription)!)
            }
        }
        
    }
    
    
    
}
