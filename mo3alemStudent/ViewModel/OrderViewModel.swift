//
//  OrderViewModel.swift
//  mo3alemStudent
//
//  Created by hassan on 9/4/18.
//  Copyright © 2018 hassan. All rights reserved.
//

import Foundation

class OrderViewModel {
    
    
    var orderSuccessedDone : ((_ message : String) -> Void)?
    var failureHappened    : ((_ message : String) -> Void)?
    
    private var _parameter = [String : String]()
    
    
    func setCode(code : String) -> Void {
        _parameter["code"] = code
    }
    
    func setParameterMakeOrder(teacherID : String , orderDate : String , orderTime : String , seassionHours : String , lat :String , lng : String , address : String , subjectId : String ) -> Void {
        
        _parameter["teacherId"] = teacherID
        _parameter["apiToken"] = UserDataActions.getApiToken()
        _parameter["order_date"] = orderDate
        _parameter["order_time"] = orderTime
        _parameter["session_hours"] = seassionHours
        _parameter["lat"] = lat
        _parameter["lng"] = lng
        _parameter["address"] = address
        _parameter["subjectId"] = subjectId
        
        
    }
    
    
    func makeOrder() -> Void {
        print(">>>> \(_parameter)")
        RestAPIRequest.SendRequest(mod: BaseModel<UserLogin>.self, urlPath: RoutesFactory.URLClear(route: .makeOrder).link, method: .post, parameter: _parameter as [String : AnyObject]) { (response) in
            switch response {
                
                
                
            case .success(let model):
                if model.status == 200 {
                    self.orderSuccessedDone!(model.message ?? "تم ارسال طلبك بنجاح")
                }else{
                    self.failureHappened!((model.error?.joined(separator: ","))!)
                }
            case .failure(let error):
                self.failureHappened!((error?.localizedDescription)!)
            }
            
            
        }
        
        
        
        
    }
    
    
    
    
    
    
    
}


