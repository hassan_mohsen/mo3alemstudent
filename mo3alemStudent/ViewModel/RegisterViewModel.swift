//
//  RegisterViewModel.swift
//  mo3alemStudent
//
//  Created by hassan on 9/2/18.
//  Copyright © 2018 hassan. All rights reserved.
//

import Foundation


class RegisterViewModel: ClousersBase {
    
    
    var successEventWithString : OperationClouserWithStrParameter?
    
    
    private var _name:String!
    private var _password : String!
    private var _phone : String!
    private var _email : String!
    private var _gender : String!
    
    
    func setData(name : String , phone : String , email : String , password : String , gender : GenderType) -> Void {
        _name = name
        _password = password
        _phone = phone
        _email = email
        _gender = gender.rawValue
    }
    
    
    
    func registerMeToApp() -> Void {
        let parameter = ["name" : _email ,
                         "email" : _email ,
                         "password" : _password ,
                         "phone" : _phone,
                         "confirm_password" : _password,
        "gender" : _gender] as [String : AnyObject]
        RestAPIRequest.SendRequest(mod: BaseModel<UserLogin>.self, urlPath: RoutesFactory.URLClear(route: .register).link, method :.post, parameter: parameter) { (response) in
            switch response {
                
            case .success(let model):
//                UserDataActions.cashUserModel(user: model.data!)
                self.successEventWithString!(model.message ?? "تم التسجيل بنجاح من فضلك قم بتنشيط ")
            case .failure(let error):
                self.failureEvent!((error?.localizedDescription)!)
            }
            
            
        }
        
    }
    
    
    
}

