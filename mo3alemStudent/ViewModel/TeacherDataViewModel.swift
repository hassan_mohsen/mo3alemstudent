//
//  TeacherDataViewModel.swift
//  mo3alemStudent
//
//  Created by hassan on 9/5/18.
//  Copyright © 2018 hassan. All rights reserved.
//

import Foundation


class TeacherDataViewModel {
    
    var successResponse : ((_ teacher :FavTeacherData) -> Void)!
    var errorResponse : ((_ message : String ) -> Void)!
    
    private var _teacherId : String!
    func setData(setID : String) -> Void {
        _teacherId = setID
    }
    
    func getTeacherData() -> Void {
        print(">>>>>>> LIMn >>> \(RoutesFactory.URLWithOneParameter(route: .teacherDetails, key: "teacherId", value: _teacherId!).link.absoluteString)")
        RestAPIRequest.SendRequest(mod: BaseModel<FavTeacherData>.self, urlPath: RoutesFactory.URLWithOneParameter(route: .teacherDetails, key: "teacherId", value: _teacherId!).link, parameter: nil) { (response) in
            switch response {
                
            case .success(let model):
                if model.status == 200 {
                    self.successResponse!(model.data!)
                }else{
                    self.errorResponse!((model.error?.joined(separator: ","))!)
                }
            case .failure(let error):
                self.errorResponse!((error?.localizedDescription)!)
            }
        }
    }
    
}



