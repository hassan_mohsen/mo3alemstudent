//
//  FavouritViewModel.swift
//  mo3alemStudent
//
//  Created by hassan on 9/5/18.
//  Copyright © 2018 hassan. All rights reserved.
//

import Foundation


class FavouritViewModel {
    
    var responseClouser : ((_ message : String) -> Void)!
    var listOfFavourites : ((_ model : [FavouriteModel]) -> Void)!
    
    
    
    private var _parameter = [String : String]()
    
    func setData(teacherID : String) -> Void {
        _parameter["teacherId"] = teacherID
        _parameter["apiToken"] = UserDataActions.getApiToken()
    }
    
    
    func changFavouritStat() -> Void {
        print("parameter Data \(_parameter)")
        RestAPIRequest.SendRequest(mod: BaseModel<String>.self, urlPath: RoutesFactory.URLClear(route: .makeFavourite).link, method :.post, parameter: _parameter as [String : AnyObject]) { (response) in
            switch response {
                
            case .success(let model):
                if model.status == 200 {
                   self.responseClouser!(model.message!)
                }else{
                    self.responseClouser!((model.error?.joined(separator: ","))!)
                }
            case .failure(let error):
                self.responseClouser!((error?.localizedDescription)!)
            }
        }
        
        
        
    }
    
    
    func getMyFavourites() -> Void {
        RestAPIRequest.SendRequest(mod: BaseModel<[FavouriteModel]>.self, urlPath: RoutesFactory.URLwithTokenParameter(route: .listFavourite).link, parameter: nil) { (response) in
            switch response {
                
            case .success(let model):
                if model.status == 200 {
                    if (model.data?.count)! > 0 {
                        self.listOfFavourites!(model.data!)
                    }else{
                        self.responseClouser!("المفضله فارغه من فضلك قم باضافه معلمين")
                    }
                    
                }else{
                    self.responseClouser!((model.error?.joined(separator: ","))!)
                }
            case .failure(let error):
                self.responseClouser!((error?.localizedDescription)!)
            }
        }
    }
    
    
}

