//
//  ActiveUserViewModel.swift
//  mo3alemStudent
//
//  Created by hassan on 9/10/18.
//  Copyright © 2018 hassan. All rights reserved.
//

import Foundation


class ActiveUserViewModel: ClousersBase {
    
    
    private var _parameter = [String : String]()
    
    var successEventWithType : ((_ type : ActiveType , _ message : String)->Void)?
    
    
    func setResendCodeParameter(email : String) -> Void {
        _parameter["email"] = email
        _parameter["type"] = "0"
    }
    
    
    func setConfirmCodeParameter(code : String , email : String) -> Void {
        
        _parameter["code"] = code
        _parameter["email"] = email
        _parameter["type"] = "0"
        
        
    }
    
    
    func resendCode() -> Void {
        
        
        
        sendReq( url: RoutesFactory.URLClear(route: .resendCode).link , type: .resendCode)
        
        /*
        RestAPIRequest.SendRequest(mod: BaseModel<String>.self, urlPath: RoutesFactory.URLClear(route: .resendCode).link, method: .post, parameter: _parameter as [String : AnyObject]) { (response) in
            switch response {

            case .success(let model):
                if model.status == 200 {
                    self.successEvent!()
                }else{
                    self.failureEvent!((model.error?.joined(separator: ","))!)
                }
            case .failure(let error):
                self.failureEvent!((error?.localizedDescription)!)
            }
        }
        */
        
        
    }
    
    
    func confirmCode() -> Void {
        
        sendReq( url: RoutesFactory.URLClear(route: .checkCode).link , type : .confirmCode)
        
    }
    
    
    private func sendReq( url : URL  , type : ActiveType ) -> Void {
        RestAPIRequest.SendRequest(mod: BaseModel<String>.self, urlPath: url, method: .post, parameter: _parameter as [String : AnyObject]) { (response) in
            switch response {
                
            case .success(let model):
                if model.status == 200 {
                    self.successEventWithType!(type, model.message ?? "تم تقديم طلبك بنجاح")
                }else{
                    self.failureEvent!((model.error?.joined(separator: ","))!)
                }
            case .failure(let error):
                self.failureEvent!((error?.localizedDescription)!)
            }
        }
    }
    
    
}

enum ActiveType {
    case resendCode
    case confirmCode
}
