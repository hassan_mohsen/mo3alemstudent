//
//  SearchReasultViewModel.swift
//  mo3alemStudent
//
//  Created by hassan on 9/3/18.
//  Copyright © 2018 hassan. All rights reserved.
//

import Foundation


typealias SuccessSearchOperation = ((_ result : [SearchResultModel]) -> Void)
typealias FailureSearchOperation = ((_ message: String) -> Void)


class SearchResultViewModel {
    
    private var _disID : String!
    private var _subID : String!
    private var _gen   : String!
    var successOperation : SuccessSearchOperation!
    var FailureOperation : FailureSearchOperation!
    
    
    func stValues(disID : String , subID : String , gender : String) -> Void {
        _disID = disID
        _subID = subID
        _gen   = gender
    }
    
    func getMeSearchResult() -> Void {
        
        RestAPIRequest.SendRequest(mod: BaseModel<[SearchResultModel]>.self, urlPath: RoutesFactory.URLSearch(disId: _disID!, subId: _subID!, gen: _gen).link, parameter: nil) { (response) in
            switch response {
                
            case .success(let model):
                self.successOperation(model.data!)
            case .failure(let error):
                self.FailureOperation((error?.localizedDescription)!)
            }
        }
        
    }
    
    
    
    
    
    
}


