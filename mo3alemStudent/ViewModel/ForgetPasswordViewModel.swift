//
//  RegisterViewModel.swift
//  Mo3alemTeacher
//
//  Created by hassan on 8/30/18.
//  Copyright © 2018 hassan. All rights reserved.
//

import Foundation


class BasePassword: ClousersBase {
    var parameter = [String : AnyObject]()
    var link : URL!
    var successEventWithStr : OperationClouserWithStrParameter?
    
    func sendRequest() -> Void {
        RestAPIRequest.SendRequest(mod: BaseModel<UserLogin>.self, urlPath: link!, method: .post, parameter: parameter) { (response) in
            switch response {
                
            case .success(let model):
                if model.status == 200 {
                    if self.successEventWithStr != nil {
                        self.successEventWithStr!(model.message ?? "تم ارسال الكود من فضلك تاكد من البريد الالكتروني")
                    }else{
                        self.successEvent!()
                    }
                    
                }else{
                    self.failureEvent!((model.error?.joined(separator: ","))!)
                }
                
            case .failure(let error):
                self.failureEvent!((error?.localizedDescription)!)
                
            }
        }
    }
}






class ForgetPasswordViewModel  : BasePassword{
    
    
    
    
    func setEmail(email:String) -> Void {
        parameter = ["email" : email ,
                     "type" : "1"] as [String : AnyObject]
        link = RoutesFactory.URLClear(route: .forgetPassword).link
    }
    
    
    
}


class EnterCodeViewModel : BasePassword {
    
    func setCodeAndEmail(code:String , email : String) -> Void {
        parameter = ["email" : email ,
                     "code" : code] as [String : AnyObject]
        link = RoutesFactory.URLClear(route: .checkCode).link
    }
    
    
}



class ResetPasswordViewModel: BasePassword {
    
    
    func setData (email : String , password : String) ->Void{
        parameter = ["email" : email ,
                     "password" : password,
                     "confirm_password" : password] as [String : AnyObject]
        link = RoutesFactory.URLClear(route: .resetPassword).link
        
    }
    
    
    
}

class ResendCodeViewModel: BasePassword {
    
    
    func setData (email : String ) ->Void{
        parameter = ["email" : email ,
                     "type" : "1"] as [String : AnyObject]
        link = RoutesFactory.URLClear(route: .resendCode).link
        
    }
    
    
    
}







