//
//  OrderDetailsViewController.swift
//  mo3alemStudent
//
//  Created by hassan on 7/12/18.
//  Copyright © 2018 hassan. All rights reserved.
//

import UIKit

class OrderDetailsViewController: UIViewController {

    
    
    @IBOutlet weak var viNumOfHours: UIView!
    @IBOutlet weak var viTotal: UIView!
    @IBOutlet weak var viCoupon: UIView!
    
    @IBOutlet weak var lblDate: UILabel!
    @IBOutlet weak var lblTime: UILabel!
    @IBOutlet weak var lblNumOfHourValue: UILabel!
    @IBOutlet weak var lblTotalValue: UILabel!
    @IBOutlet weak var lblHourPriceMsg: UILabel!
    @IBOutlet weak var txtCoupon: UITextField!
    @IBOutlet weak var lblName: UILabel!
    
    
    var numberOfHour : String!
    var totalPrice : String!
    var name : String!
    var time : String!
    var date : String!
    
    
    var orderViewModel : OrderViewModel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        viNumOfHours.layer.borderColor = #colorLiteral(red: 0.8, green: 0.8, blue: 0.8, alpha: 1).cgColor
        viTotal.layer.borderColor = #colorLiteral(red: 0.8, green: 0.8, blue: 0.8, alpha: 1).cgColor
        viCoupon.layer.borderColor = #colorLiteral(red: 0.8, green: 0.8, blue: 0.8, alpha: 1).cgColor
        lblDate.layer.borderColor = #colorLiteral(red: 0.8, green: 0.8, blue: 0.8, alpha: 1).cgColor
        lblTime.layer.borderColor = #colorLiteral(red: 0.8, green: 0.8, blue: 0.8, alpha: 1).cgColor
        
        orderViewModel.failureHappened = { (message) in
            self.showErrorWithMessage(message: message)
        }
        orderViewModel.orderSuccessedDone = { (message) in
//            self.showAlertWithMessageAndTitle(title: "تم", message: message)
            self.showAlertWithTitleAndMessageWith1Handler(title: "تم", message: message, doneTitle: "تم", doneHandler: { (alert) in
                self.navigationController?.popToRootViewController(animated: true)
            })
        }

        
        
        lblName.text = name
        lblTotalValue.text = "\(totalPrice!) ريال"
        lblNumOfHourValue.text = numberOfHour
        lblTime.text = "\(time!) ساعة"
        lblDate.text = date
        
        
        
        
        
    }

    @IBAction func btnOrderAct(_ sender: Any) {
        if !txtCoupon.textNoNil().isEmpty {
            orderViewModel.setCode(code: txtCoupon.textNoNil())
        }
        
        orderViewModel.makeOrder()
        
    }
    
}
