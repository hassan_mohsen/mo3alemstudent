//
//  LoginViewController.swift
//  mo3alemStudent
//
//  Created by hassan on 6/21/18.
//  Copyright © 2018 hassan. All rights reserved.
//

import UIKit

class LoginViewController: UIViewController {

    
    @IBOutlet weak var txtEmail: UITextField!
    @IBOutlet weak var txtPassword: UITextField!
    
    private var loginModel = LoginViewModel()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        
        loginModel.successEvent = {
            // success Event
            print("success event")
            let vc = HomeTabBarViewController()
            self.present(vc, animated: true, completion: nil)
        }
        
        loginModel.failureEvent = { [unowned self] message in
            self.showErrorWithMessage(message: message)
        }
        loginModel.notActiveEvent = { [weak self] message in
            self?.showAlertWithTitleAndMessageWith1Handler(title: "تحذير", message: message, doneTitle: "تم", doneHandler: { (alert) in
                let vc = self?.getViewControllerFromStroyBoard(StoryBoard: "Main", bund: nil  , identifierVC: "ActiveUserViewController") as! ActiveUserViewController
                vc.email = self?.txtEmail.textNoNil()
                self?.present(vc, animated: true, completion: nil)
            })
        }
        
        
    }

    @IBAction func btnLoginAct(_ sender: Any) {
        
        if txtEmail.textNoNil().count == 0 {
            showErrorWithMessage(message: "من فضلك ادخل البريد الالكتروني .")
            return
        }
        
        
        
        if txtPassword.textNoNil().count == 0  {
            showErrorWithMessage(message: "من فضلك ادخل كلمه المرور.")
            return
        }
        
        if !(txtEmail.text?.isValidEmail())!{
            showErrorWithMessage(message: "من فضلك ادخل بريد الكتروني صحيح .")
            return
        }
        
        
        loginModel.setModelValues(email: txtEmail.textNoNil(), pass: txtPassword.textNoNil())
        
        loginModel.makeMeLogin()
        
        
        
    }
    
    @IBAction func btnRegisterAct(_ sender: Any) {
        let vc = getViewControllerFromStroyBoard(StoryBoard: "Main", bund: nil, identifierVC: "RegStudentViewController") as! RegStudentViewController
        vc.parentVC = self
        present(vc, animated: true, completion: nil)
     }
    
    @IBAction func btnForgetAct(_ sender: Any) {
        
        let vc = getViewControllerFromStroyBoard(StoryBoard: "Main", bund: nil, identifierVC: "ForgetPasswordViewController") as! ForgetPasswordViewController
        vc.parentVC = self
        present(vc, animated: true, completion: nil)
        
        
    }
}
