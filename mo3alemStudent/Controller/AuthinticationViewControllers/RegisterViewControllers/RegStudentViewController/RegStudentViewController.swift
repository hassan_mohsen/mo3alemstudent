//
//  RegStudentViewController.swift
//  mo3alemStudent
//
//  Created by hassan on 6/21/18.
//  Copyright © 2018 hassan. All rights reserved.
//

import UIKit

class RegStudentViewController: MainRegViewController {

    
    var registerViewModel = RegisterViewModel()
    
    var parentVC : UIViewController!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        registerViewModel.successEventWithString = { [weak self] (message) in
                
                self?.showAlertWithTitleAndMessageWith1Handler(title: "تم", message: message, doneTitle: "تم", doneHandler: { (alert) in
                    
                    let vc = self?.getViewControllerFromStroyBoard(StoryBoard: "Main", bund: nil, identifierVC: "ActiveUserViewController") as! ActiveUserViewController
                    vc.email = self?.txtEmail.textNoNil()
                    self?.dismiss(animated: true, completion: nil)
                    self?.parentVC.present(vc, animated: true, completion: nil)
                    
                })
        }
        
        
        registerViewModel.failureEvent = { (message) in
            self.showErrorWithMessage(message: message)
        }
        
    }

    
    @IBAction func femaleStudentAct(_ sender: Any) {
        selectedGenderType(but: femaleGenderBut, unselect: maleGenderBut, gender: .Female)
    }
    @IBAction func maleStudentAct(_ sender: Any) {
        selectedGenderType(but: maleGenderBut , unselect: femaleGenderBut, gender: .Male)
    }
    
    
    @IBAction func regAsTeacher(_ sender: Any) {
        let vc = getViewControllerFromStroyBoard(StoryBoard: "Main", bund: nil, identifierVC: "RegTeacherViewController") as! RegTeacherViewController
        
        present(vc, animated: true , completion: nil)
        
    }
    @IBAction func closeButAct(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    
    
    @IBAction func RegStudentAct(_ sender: Any) {
        
        if txtUserName.textNoNil().count == 0 {
            showErrorWithMessage(message: "من فضلك ادخل اسم المستخدم . ")
            return
        }
        if txtPhone.textNoNil().count == 0 {
            showErrorWithMessage(message: "من فضلك ادخل رقم الهاتف")
            return
        }
        if txtEmail.textNoNil().count == 0 {
            showErrorWithMessage(message: "من فضلك ادخل البريد الالكتروني .")
            return
        }
        if txtPassword.textNoNil().count == 0{
            showErrorWithMessage(message: "من فضلك ادخل كلمة المرور .")
            return
        }
        if txtRePassword.textNoNil().count == 0 {
            showErrorWithMessage(message: "من فضلك ادخل تكرار كلمه المرور.")
            return
        }
        if genderType == nil {
            showErrorWithMessage(message: "من فضلك اختر نوع الطالب .")
            return
        }
        if !(txtEmail.text?.isValidEmail())! {
            showErrorWithMessage(message: "من فضلك ادخل بريد الكتروني صحيح .")
            return
        }
        if txtPassword.text != txtRePassword.text {
            showErrorWithMessage(message: "كلمه المرور وتكرار كلمه المرور غير متطابقتين")
        }
        
        registerViewModel.setData(name: txtUserName.textNoNil(), phone: txtPhone.textNoNil(), email: txtEmail.textNoNil(), password: txtPassword.textNoNil(), gender: genderType!)
        registerViewModel.registerMeToApp()
        
        
    }
    
    
}
