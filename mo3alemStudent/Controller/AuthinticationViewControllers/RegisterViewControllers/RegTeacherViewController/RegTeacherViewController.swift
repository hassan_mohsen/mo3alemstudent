//
//  RegTeacherViewController.swift
//  mo3alemStudent
//
//  Created by hassan on 6/21/18.
//  Copyright © 2018 hassan. All rights reserved.
//

import UIKit

class RegTeacherViewController: MainRegViewController {

    
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
    }

    
    
    @IBAction func maleTeacherAct(_ sender: Any) {
        selectedGenderType(but: maleGenderBut, unselect: femaleGenderBut, gender: .Male)
    }
    @IBAction func femaleTeacherAct(_ sender: Any) {
        selectedGenderType(but: femaleGenderBut, unselect: maleGenderBut, gender: .Female)
    }
    
    
    
    @IBAction func closeButAct(_ sender: Any) {
        dismiss(animated: true , completion: nil)
    }
    
    

}
