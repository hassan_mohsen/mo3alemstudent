//
//  EnterCodeViewController.swift
//  Mo3alemTeacher
//
//  Created by hassan on 8/30/18.
//  Copyright © 2018 hassan. All rights reserved.
//

import UIKit

class EnterCodeViewController: UIViewController {

    var parentVC : UIViewController!
    var userEmail : String!
    
    @IBOutlet weak var txtCode: HMTextField!
    
    var codeViewModel = EnterCodeViewModel()
    var reSendViewModel = ResendCodeViewModel()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        
        codeViewModel.successEvent = {
            let vc = self.getViewControllerFromStroyBoard(StoryBoard: "Main", bund: nil, identifierVC: "NewPasswordViewController") as! NewPasswordViewController
            vc.userEmail = self.userEmail!
            self.dismiss(animated: true , completion: nil)
            self.parentVC.present(vc, animated: true, completion: nil)
        }
        
        codeViewModel.failureEvent = { (message) in
            
            self.showErrorWithMessage(message: message)
        }
        
        
        reSendViewModel.successEventWithStr = {[weak self] (message) in
            self?.showAlertWithMessageAndTitle(title: "تم", message: message)
        }
        reSendViewModel.successEvent = {
            self.showAlertWithMessageAndTitle(title: "تم", message: "تم ارسال الكود بنجاح من فضلك تاكد من البريد الالكتروني")
        }
        reSendViewModel.failureEvent = {(message) in
            self.showErrorWithMessage(message: message)
        }
    
    
    }

    
    @IBAction func reSendCode(_ sender: Any) {
        reSendViewModel.setData(email: userEmail!)
        reSendViewModel.sendRequest()
    }
    
    @IBAction func SubmitCode(_ sender: Any) {
        
        if txtCode.text == nil {
            showErrorWithMessage(message: "من فضلك ادخل الكود المرسل اليك .")
            return
        }
        
        codeViewModel.setCodeAndEmail(code: txtCode.text!, email: userEmail!)
        codeViewModel.sendRequest()
        
    }
    
    

}
