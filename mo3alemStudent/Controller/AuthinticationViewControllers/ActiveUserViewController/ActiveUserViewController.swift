//
//  ActiveUserViewController.swift
//  mo3alemStudent
//
//  Created by hassan on 9/10/18.
//  Copyright © 2018 hassan. All rights reserved.
//

import UIKit

class ActiveUserViewController: UIViewController {

    
    @IBOutlet weak var lblEmail: UILabel!
    
    @IBOutlet weak var txtCode: HMTextField!
    
    var email : String!
    var activeViewModel = ActiveUserViewModel()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        lblEmail.text = email
        
        activeViewModel.successEventWithType = { [weak self] (type , message) in
            
            switch type {
            
            case .resendCode:
                self?.showAlertWithMessageAndTitle(title: "تم", message: message)
            case .confirmCode:
                self?.showAlertWithTitleAndMessageWith1Handler(title: "تم", message: message, doneTitle: "تم", doneHandler: { (alert) in
                    self?.dismiss(animated: true, completion: nil)
                })
            }
            
        }
        
        
    }

    @IBAction func confirmCodeAct(_ sender: Any) {
        if txtCode.textNoNil().isEmpty {
            showErrorWithMessage(message: "من فضلك ادخل الكود المرسل اليك")
            return
        }
        
        activeViewModel.setConfirmCodeParameter(code: txtCode.textNoNil(), email: email!)
        activeViewModel.confirmCode()
        
    }
    
    @IBAction func reSendCodeAct(_ sender: Any) {
        activeViewModel.setResendCodeParameter(email: email!)
        activeViewModel.resendCode()
    }
    
    
    @IBAction func btnCloseAct(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    

}
