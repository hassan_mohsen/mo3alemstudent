//
//  SearchViewController.swift
//  mo3alemStudent
//
//  Created by hassan on 6/25/18.
//  Copyright © 2018 hassan. All rights reserved.
//

import UIKit

class SearchViewController: UIViewController {

    
    
    
    @IBOutlet weak var imgGender: UIImageView!
    @IBOutlet weak var lblCity: UILabel!
    @IBOutlet weak var lblRegion: UILabel!
    @IBOutlet weak var lblStudyStep: UILabel!
    @IBOutlet weak var lblSubject: UILabel!
    
    var selectedType : GenderType = .Female
    
    var citiesMode : [CitiesModel]?
    var regionsModel : [CitiesModel]?
    var stagesModel : [StageModel]?
    var subject : [SubjectModel]?
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        RestAPIRequest.SendRequest(mod: BaseModel<[CitiesModel]>.self, urlPath: RoutesFactory.URLClear(route: .cities).link, parameter: nil) { (response) in
            switch response {
                
            case .success(let model):
                self.citiesMode = model.data
            case .failure(let error):
                self.showErrorWithMessage(message: (error?.localizedDescription)!)
            }
        }
        
        
        RestAPIRequest.SendRequest(mod: BaseModel<[StageModel]>.self, urlPath: RoutesFactory.URLClear(route: .stages).link, parameter: nil) { (response) in
            switch response {
                
            case .success(let model):
                self.stagesModel = model.data
            case .failure(let error):
                self.showErrorWithMessage(message: (error?.localizedDescription)!)
            }
        }
        
        
        
        
    }

    
    
    @IBAction func genderButAct(_ sender: Any) {
        let vc = getViewControllerFromStroyBoard(StoryBoard: "Main", bund: nil, identifierVC: "ChooseGenderPopUp") as! ChooseGenderPopUp
        vc.delegate = self
        present(vc, animated: true, completion: nil)
    }
    
    @IBAction func cityButAct(_ sender: Any) {
        if citiesMode != nil  {
            
            let arrString = citiesMode!.map({ (Model) -> String in
                return Model.name!
            })
            if arrString.count == 0 {
                showErrorWithMessage(message: "لا يوجد مدن للعرض .")
                return
            }
            McPicker.show(data: [arrString], doneHandler: { [weak self](selection : [Int : String]) in
                let selectedValue = selection[0]
                print("value >>> \(selectedValue!)")
                let index = arrString.index(of: selectedValue!)!
                self?.lblCity.text = selectedValue
                self?.lblCity.tag = (self?.citiesMode![index].id)!
                
                RestAPIRequest.SendRequest(mod: BaseModel<[CitiesModel]>.self, urlPath: RoutesFactory.URLWithOneParameter(route: .region, key: "cityId", value: "\(self!.citiesMode![index].id!))").link, parameter: nil, completion: { (response) in
                    switch response {
                        
                    case .success(let model):
                        self?.regionsModel = model.data
                    case .failure(let error):
                        self?.showErrorWithMessage(message: (error?.localizedDescription)!)
                    }
                })
                
                
            })
            
            
        }else{
            showErrorWithMessage(message: "لم يتم تحميل المدن للعرض")
        }
    }
    
    @IBAction func regionButAct(_ sender: Any) {
        if regionsModel != nil {
            
            let pickArr = regionsModel!.map({ (model) -> String in
                return model.name!
            })
            
            if pickArr.count == 0 {
                showErrorWithMessage(message: "لا يوجد مناطق في هذه المدينه للعرض .")
                return
            }
            McPicker.show(data: [pickArr], doneHandler: { [weak self](selection :[Int : String]) in
                let selectedStr = selection[0]
                self?.lblRegion.text = selectedStr
                let index = pickArr.index(of: selectedStr!)!
                let selectedModel = self?.regionsModel![index]
                self?.lblRegion.tag = (selectedModel?.id)!
                
            })
            
            
            
        }else{
            showErrorWithMessage(message: "لا يوجد مناطق للعرض")
        }
    }
    
    @IBAction func studyStepButAct(_ sender: Any) {
        
        if stagesModel != nil {
            
            let pickerData = stagesModel!.map({ (model) -> String in
                return model.name!
            })
            
            if pickerData.count == 0 {
                showErrorWithMessage(message: "لا يوجد مراحل دراسيه للعرض .")
                return
            }
            
            
            McPicker.show(data: [pickerData], doneHandler: { [weak self] (selection : [Int : String]) in
                
                let selectedStr = selection[0]
                self?.lblStudyStep.text = selectedStr
                let index = pickerData.index(of: selectedStr!)!
                let selectedObj = self?.stagesModel![index]
                self?.lblStudyStep.tag = (selectedObj?.id)!
                RestAPIRequest.SendRequest(mod: BaseModel<[SubjectModel]>.self, urlPath: RoutesFactory.URLWithOneParameter(route: .subject, key: "stageId", value: "\((selectedObj?.id)!)").link, parameter: nil, completion: { (response) in
                    switch response {
                        
                    case .success(let model):
                        self?.subject = model.data
                    case .failure(let error):
                        self?.showErrorWithMessage(message: (error?.localizedDescription)!)
                    }
                })
                
                
            })
            
            
            
        }else{
            showErrorWithMessage(message: "لم يتم تحميل المراحل الدراسيه بعد .")
            
        }
        
    }
    
    @IBAction func subjectButAct(_ sender: Any) {
        
        if subject != nil {
           
            let pickerData = subject!.map({ (model) -> String in
                return model.name!
            })
            
            if pickerData.count == 0 {
                showErrorWithMessage(message: "لا يوجد مواد دراسيه في هذه المرحله .")
                return
            }
            
            McPicker.show(data: [pickerData], doneHandler: {[weak self] (selection : [Int : String]) in
                
                let selected = selection[0]
                self?.lblSubject.text = selected
                let index = pickerData.index(of: selected!)!
                self?.lblSubject.tag = (self?.subject![index].id)!
                
                
            })
            
            
        }else{
            showErrorWithMessage(message: "لم يتم تحميل المواد بعد .")
        }
        
        
    }
    
    
    @IBAction func searchButAct(_ sender: Any) {
        
        if lblRegion.tag == 0 {
            showErrorWithMessage(message: "من فضلك اختر الحي اولآ")
            return
        }
        if lblSubject.tag == 0 {
            showErrorWithMessage(message: "من فضلك اختر الماده اولآ .")
            return
        }
        
        let vc = getViewControllerFromStroyBoard(StoryBoard: "Main", bund: nil, identifierVC: "SearchResultViewController") as! SearchResultViewController
        vc.disID = "\(lblRegion.tag)"
        vc.subID = "\(lblSubject.tag)"
        vc.genType = "\(selectedType.hashValue)"
        let nav = UINavigationController.init(rootViewController: vc)
        self.present(nav, animated: true, completion: nil)
        
    }
    
    
    
}
extension SearchViewController : ChooseGendetDelegate
{
    func genderType(type: GenderType) {
        selectedType = type
        imgGender.image = UIImage.init(named: type.rawValue)
        
    }
    
    
}
