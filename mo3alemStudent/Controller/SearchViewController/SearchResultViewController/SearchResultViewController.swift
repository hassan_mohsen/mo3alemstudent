//
//  SearchResultViewController.swift
//  mo3alemStudent
//
//  Created by hassan on 6/28/18.
//  Copyright © 2018 hassan. All rights reserved.
//

import UIKit

class SearchResultViewController: UIViewController {

    @IBOutlet weak var tblResult: UITableView!
    
    var tableData = [SearchResultModel](){
        didSet{
            tblResult.reloadData()
            
        }
    }
    
    
    var disID : String!
    var subID : String!
    var genType : String!
    
    var searchResultViewModel  = SearchResultViewModel()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        title = "نتائج البحث"
        //dis=12&sub=19
//        disID = "12"
//        subID = "19"
        
        self.navigationController?.navigationBar.barTintColor = #colorLiteral(red: 0.1137254902, green: 0.7450980392, blue: 0.8156862745, alpha: 1)
        self.navigationController?.navigationBar.tintColor = .white
        
        let backBut  = UIBarButtonItem.init(image: #imageLiteral(resourceName: "leftArrow"), style: .plain, target: self, action: #selector(backButAct))
        self.navigationItem.leftBarButtonItem = backBut
        
        
        tblResult.register(UINib.init(nibName: "UsersListCustomCell", bundle: nil), forCellReuseIdentifier: "cell")
        
        
        
        searchResultViewModel.successOperation = { (resultModel) in
            if resultModel.count == 0 {
                self.showErrorWithMessage(message: "لا يوجد نتائج مطابقه لبحثك .")
            }else{
                
                self.tableData = resultModel
                
            }
        }
        
        searchResultViewModel.FailureOperation = { (message) in
            self.showErrorWithMessage(message: message)
        }
        
        searchResultViewModel.stValues(disID: disID!, subID: subID!, gender: genType!)
        
        
    }

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        if tableData.count == 0 {
            searchResultViewModel.getMeSearchResult()
        }
        
    }
    
    
    
    @objc func backButAct() -> Void {
        self.navigationController?.dismiss(animated: true, completion: nil)
    }
    

}


extension SearchResultViewController : UITableViewDataSource
{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return tableData.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! UsersListCustomCell
        
        
        let obj = tableData[indexPath.row]
        
        
        
        cell.cellLblName.text = obj.name
        cell.cellLblNation.text = obj.nationality
        cell.cellViRate.rating = Double(obj.avgRate!)
        cell.cellLblPriceForHour.text = "\(doubleToInteger(doubleToString: "\(obj.teacherSubjects!.price!)"))"
        
        
        cell.cellBtnCV.tag = indexPath.row
        cell.cellBtnOrder.tag = indexPath.row
        
        cell.cellBtnCV.addTarget(self, action: #selector(showCVAct(but:)), for: .touchUpInside)
        cell.cellBtnOrder.addTarget(self, action: #selector(orderAct(but:)), for: .touchUpInside)
        
        
        if indexPath.row % 2 != 0 {
            cell.backgroundColor = .white
        }
        return cell
    }
    
    @objc func showCVAct(but : UIButton) -> Void {
        
        let teacher = tableData[but.tag]
        
        guard let url = URL(string: "https://stackoverflow.com") else { return }
        UIApplication.shared.open(url)

    }
    @objc func orderAct(but :UIButton) -> Void {
        
        let teacher = tableData[but.tag]
        
        let vc = getViewControllerFromStroyBoard(StoryBoard: "Main", bund: nil, identifierVC: "NewOrderViewController") as! NewOrderViewController
        vc.teacherData = teacher
        self.navigationController?.pushViewController(vc, animated: true)
    }
    func doubleToInteger(doubleToString:String)-> Int {
        
        let stringToInteger = (doubleToString as NSString).integerValue
        
        return stringToInteger
    }
}


extension SearchResultViewController : UITableViewDelegate
{
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        
        let vc = getViewControllerFromStroyBoard(StoryBoard: "Main", bund: nil, identifierVC: "TeacherProfileViewController") as! TeacherProfileViewController
        vc.teacherModel = tableData[indexPath.row]
        self.navigationController?.pushViewController(vc, animated: true)
//        self.present(vc, animated: true, completion: nil)
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 90
    }
    
}



