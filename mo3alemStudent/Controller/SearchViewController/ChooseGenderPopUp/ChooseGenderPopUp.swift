//
//  ChooseGenderPopUp.swift
//  mo3alemStudent
//
//  Created by hassan on 6/27/18.
//  Copyright © 2018 hassan. All rights reserved.
//

import UIKit

let CONST_VALUE : CGFloat = 35.0


protocol ChooseGendetDelegate {
    func genderType(type : GenderType) -> Void
}

class ChooseGenderPopUp: UIViewController {

    @IBOutlet weak var viBottom: UIView!
    @IBOutlet weak var constBottonGender: NSLayoutConstraint!
    var delegate : ChooseGendetDelegate!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        
        
    }

    
    
    
    
    @IBAction func dismisButAct(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    
    @IBAction func btnUpperACt(_ sender: Any) {
        delegate.genderType(type: .Female)
        
        UIView.animate(withDuration: 1.5, animations: {
            self.view.backgroundColor = .clear
            self.viBottom.alpha = 0
        }) { (done) in
            self.viBottom.isHidden = true
            if done {
                self.dismiss(animated: true, completion: nil)
            }
        }
        
        
    }
    @IBAction func btnBottomAct(_ sender: Any) {
        
        delegate.genderType(type: .Male)
        constBottonGender.constant = CONST_VALUE
        
        /*
        UIView.animate(withDuration: 1, delay: 0, usingSpringWithDamping: 0, initialSpringVelocity: 5, options: .curveEaseInOut, animations: {
            self.view.layoutIfNeeded()
        }) { (finish) in
            if finish {
                self.dismiss(animated: true, completion: nil)
            }
        }
        */
        
        UIView.animate(withDuration: 1.0, animations: {
            self.view.layoutIfNeeded()
        }) { (done) in
            if done {
                self.dismiss(animated: true, completion: nil)
            }
        }
 
    }
    
}
