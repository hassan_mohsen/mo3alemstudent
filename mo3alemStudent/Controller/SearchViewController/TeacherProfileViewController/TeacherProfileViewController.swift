//
//  TeacherProfileViewController.swift
//  mo3alemStudent
//
//  Created by hassan on 7/2/18.
//  Copyright © 2018 hassan. All rights reserved.
//

import UIKit
import Cosmos

// isAvailablity > 1 is available >> 0 not available


class TeacherProfileViewController: UIViewController {

    //===================================
    @IBOutlet weak var imgProfile: UIImageView!
    @IBOutlet weak var lblTeacherName: UILabel!
    @IBOutlet weak var lblSubTitle: UILabel!
    @IBOutlet weak var viRate: CosmosView!
    @IBOutlet weak var lblNation: UILabel!
    @IBOutlet weak var lblAvilability: UILabel!
    @IBOutlet weak var lblPricePerHour: UILabel!
    //===================================
    @IBOutlet weak var imgNation: UIImageView!
    @IBOutlet weak var imgAvailabilty: UIImageView!
    @IBOutlet weak var imgPrice: UIImageView!
    
    //===================================
    
    var teacherModel : SearchResultModel!
    var teacherData : FavTeacherData?
    override func viewDidLoad() {
        super.viewDidLoad()
        title = "الملف الشخصي"
        let backBut  = UIBarButtonItem.init(image: #imageLiteral(resourceName: "leftArrow"), style: .plain, target: self, action: #selector(backButAct))
        self.navigationItem.leftBarButtonItem = backBut
        
        
        if let mod = teacherData {
            let teacherData = mod
            
            imgProfile.image = teacherData.gender == 0 ? UIImage.init(named: "MALE") : UIImage.init(named: "FEMALE")
            imgProfile.moa.url = teacherData.image
            lblNation.text = teacherData.nationality
            //        lblSubTitle.text
            lblAvilability.text = teacherData.isAvailable == 1 ? "متاح" : "غير متاح"
            imgAvailabilty.image = teacherData.isAvailable == 1 ? #imageLiteral(resourceName: "ProfileStateAvaliable") : UIImage.init(named: "ProfileStateUnavaliable")
            lblTeacherName.text = teacherData.name
            viRate.rating = Double(teacherData.avgRate ?? 0)
//            lblPricePerHour.text = "\((teacherData.teacherSubjects?.subjectPrice)!)"
            
        }else{
            imgProfile.image = teacherModel.gender == 0 ? UIImage.init(named: "MALE") : UIImage.init(named: "FEMALE")
            imgProfile.moa.url = teacherModel.image
            lblNation.text = teacherModel.nationality
            //        lblSubTitle.text
            lblAvilability.text = teacherModel.isAvailable == 1 ? "متاح" : "غير متاح"
            imgAvailabilty.image = teacherModel.isAvailable == 1 ? #imageLiteral(resourceName: "ProfileStateAvaliable") : UIImage.init(named: "ProfileStateUnavaliable")
            lblTeacherName.text = teacherModel.name
            viRate.rating = Double(teacherModel.avgRate ?? 0)
            lblPricePerHour.text = "\((teacherModel.teacherSubjects?.price)!)"
            
        }
        
        
        
        
    }

    @IBAction func makeFavouritAct(_ sender: Any) {
        let changeFavouritViewModel = FavouritViewModel()
        changeFavouritViewModel.responseClouser = { [weak self] (message) in
            
            self?.showAlertWithMessageAndTitle(title: "رسالة", message: message)
            
        }
        changeFavouritViewModel.setData(teacherID: "\(teacherModel.id!)")
        changeFavouritViewModel.changFavouritStat()
    }
    @IBAction func orderNowButAct(_ sender: Any) {
        
        let vc = getViewControllerFromStroyBoard(StoryBoard: "Main", bund: nil, identifierVC: "NewOrderViewController") as! NewOrderViewController
        vc.teacherData = teacherModel!
        self.navigationController?.show(vc, sender: nil)
//        self.present(vc, animated: true, completion: nil)
    }
    @IBAction func cvButAct(_ sender: Any) {
        
        
        let vc = getViewControllerFromStroyBoard(StoryBoard: "Main", bund: nil, identifierVC: "ShowCVViewController") as! ShowCVViewController
        vc.teacherModel = teacherModel
        vc.teacherData = teacherData
        self.navigationController?.show(vc, sender: nil)
        
        
        
    }
    
    
    @objc func backButAct() -> Void {
        self.navigationController?.popViewController(animated: true)
    }
    
}
