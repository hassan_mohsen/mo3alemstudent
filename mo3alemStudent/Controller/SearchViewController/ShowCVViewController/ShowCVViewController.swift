//
//  ShowCVViewController.swift
//  mo3alemStudent
//
//  Created by hassan on 9/8/18.
//  Copyright © 2018 hassan. All rights reserved.
//

import UIKit

class ShowCVViewController: UIViewController {

    
    @IBOutlet weak var viInfo: UIView!
    @IBOutlet weak var viCertif: UIView!
    
    @IBOutlet weak var txtInfo: UITextView!
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var imgProfile: UIImageView!
    
    var teacherModel : SearchResultModel!
    var teacherData : FavTeacherData?
    
    var educationCertURL : String!
    var graduationCertURL : String!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        //===========================================
        viInfo.layer.borderColor = UIColor.darkGray.cgColor
        viCertif.layer.borderColor = UIColor.darkGray.cgColor
        //===========================================
        
        if let mod = teacherData {
            let teacherData = mod
            
            imgProfile.image = teacherData.gender == 0 ? UIImage.init(named: "MALE") : UIImage.init(named: "FEMALE")
            imgProfile.moa.url = teacherData.image
            txtInfo.text = teacherData.experience ?? "لا تتوفر اي نبذه حاليآ"
            lblName.text = teacherData.name
            educationCertURL = teacherData.teacherCertificate?.universityCertificate ?? ""
            graduationCertURL = teacherData.teacherCertificate?.otherCertificate ?? ""
            
        }else{
            txtInfo.text = teacherModel.experience ?? "لا تتوفر اي نبذه حاليآ"
            imgProfile.image = teacherModel.gender == 0 ? UIImage.init(named: "MALE") : UIImage.init(named: "FEMALE")
            imgProfile.moa.url = teacherModel.image
            lblName.text = teacherModel.name
            
            educationCertURL = teacherModel.teacherCertificate?.universityCertificate ?? ""
            graduationCertURL = teacherModel.teacherCertificate?.otherCertificate ?? ""

        }
        
        
        
    }
    @IBAction func UniversityAct(_ sender: Any) {
        guard let url = URL(string: educationCertURL) else { return }
        UIApplication.shared.open(url)

    }
    @IBAction func GraduationAct(_ sender: Any) {
        guard let url = URL(string:graduationCertURL ) else { return }
        UIApplication.shared.open(url)
    }
    
   
}
