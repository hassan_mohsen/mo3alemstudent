
//
//  MyOrderViewController.swift
//  mo3alemStudent
//
//  Created by hassan on 6/25/18.
//  Copyright © 2018 hassan. All rights reserved.
//

import UIKit

class MyOrderViewController: UIViewController {

    
    @IBOutlet weak var tblMyOrder: UITableView!
    
    var tableData = [[OrderNewModel]]() {
        didSet{
//            tblMyOrder.reloadData()
        }
    }
    var myOrderViewModel = MyOrderListViewModel()
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationItem.title = "طلباتي"
        
        tblMyOrder.register(UINib.init(nibName: "MyOrderCustomCell", bundle: nil), forCellReuseIdentifier: "cell")
        
        
        myOrderViewModel.SuccessResponse = {[weak self] (model) in
            self?.tableData = model
            self?.tblMyOrder.reloadData()
        }
        myOrderViewModel.FailurResponse = { [weak self] (message) in
            self?.showErrorWithMessage(message: message)
        }
        
        myOrderViewModel.GetAllMyOrders()
        
    }

    

}

extension MyOrderViewController : UITableViewDataSource
{
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! MyOrderCustomCell
        
        let obj = tableData[indexPath.section][indexPath.row]
        
        cell.cellLbltitle.text = obj.teacher?.name
        
        
        
        if let dateAndTime = obj.createdAt?.split(separator: " ") {
            cell.cellLblDate.text = "\(dateAndTime[0])"
            cell.cellLblTime.text = "\(dateAndTime[1])"
        }
        
        
        
        
        
        
        cell.cellBack.layer.cornerRadius = 5.0
        cell.cellBack.layer.masksToBounds = true
        cell.cellDarkBack.layer.cornerRadius = 5.0
        cell.cellDarkBack.layer.masksToBounds = true
        
        switch (obj.status)! {
        case 0:
            print("0")
            cell.cellBack.backgroundColor = #colorLiteral(red: 0.4941176471, green: 0.5411764706, blue: 0.6274509804, alpha: 1)
            cell.cellDarkBack.backgroundColor = #colorLiteral(red: 0.4029241655, green: 0.4418566158, blue: 0.5167199247, alpha: 1)
            cell.cellLblStatus.text = "معلق"
            cell.cellimgStatus.image = UIImage.init(named: "OrderSuspend")
        case 1:
            print("1")
            cell.cellBack.backgroundColor = #colorLiteral(red: 0.07843137255, green: 0.7490196078, blue: 0.5137254902, alpha: 1)
            cell.cellDarkBack.backgroundColor = #colorLiteral(red: 0.06719979398, green: 0.5788682312, blue: 0.391923528, alpha: 1)
            cell.cellLblStatus.text = "موافق"
            cell.cellimgStatus.image = UIImage.init(named: "OrderAccepted")
        case -1:
            print("-1")
            cell.cellBack.backgroundColor = #colorLiteral(red: 1, green: 0.4235294118, blue: 0.4352941176, alpha: 1)
            cell.cellDarkBack.backgroundColor = #colorLiteral(red: 0.7978060233, green: 0.3427767442, blue: 0.3580168412, alpha: 1)
            cell.cellLblStatus.text = "مرفوض"
            cell.cellimgStatus.image = UIImage.init(named: "OrderRefused")
        case -2:
            print("-2")
            cell.cellBack.backgroundColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
            cell.cellLblStatus.text = "ملغاه"
            cell.cellimgStatus.image = UIImage.init(named: "OrderRefused")
        default:
            print("Default")
        }
        
        if indexPath.section == 1 {
            cell.cellBack.backgroundColor = #colorLiteral(red: 0.8980392157, green: 0.8980392157, blue: 0.8980392157, alpha: 1)
        }
        
        cell.layer.cornerRadius = 5.0
        cell.layer.masksToBounds = true
        cell.backgroundColor = .clear
        
        return cell
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return tableData.count
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return tableData[section].count
    }
}


extension MyOrderViewController : UITableViewDelegate
{
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        
        var obj = tableData[indexPath.section][indexPath.row]
        if obj.cellHeight == ExpandTableHeight.collaps.rawValue {
            obj.cellHeight = ExpandTableHeight.Expand.rawValue
        }else{
            obj.cellHeight = ExpandTableHeight.collaps.rawValue
        }
        tableData[indexPath.section][indexPath.row] = obj
        
        tableView.reloadRows(at: [indexPath], with: .none)
        tableView.scrollToRow(at: indexPath, at: .middle, animated: true)
        
        
        
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        
        let vi = UIView.init(frame: CGRect.init(x: 0, y: 0, width: tableView.bounds.width, height: 40))
        vi.backgroundColor = .clear
        
        let lbl = UILabel.init(frame: vi.bounds)
        lbl.textAlignment = .right
        lbl.textColor = .darkGray
        lbl.text = "سابقآ"
        
        vi.addSubview(lbl)
        
        
        if section == 0 {
            lbl.text = "اخر 24 ساعة"
            
            let but = UIButton.init(frame: CGRect.init(x: 16, y: 4, width: 100, height: 30))
            but.backgroundColor = #colorLiteral(red: 0.9411764706, green: 0.7843137255, blue: 0.3803921569, alpha: 1)
            but.setTitle("مفضلتي", for: .normal)
            but.setTitleColor(.white, for: .normal)
            but.addTarget(self, action: #selector(MyFavourit), for: .touchUpInside)
            but.layer.cornerRadius = 5.0
            but.layer.masksToBounds = true
            vi.addSubview(but)
            
        }
        vi.backgroundColor = UIColor.groupTableViewBackground
        return vi
        
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return tableData[indexPath.section][indexPath.row].cellHeight
    }
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 50
    }
    
    @objc func MyFavourit() -> Void {
        let vc = getViewControllerFromStroyBoard(StoryBoard: "Main", bund: nil, identifierVC: "FavouritViewController") as! FavouritViewController
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    
}



