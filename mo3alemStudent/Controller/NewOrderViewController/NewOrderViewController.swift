//
//  NewOrderViewController.swift
//  mo3alemStudent
//
//  Created by hassan on 7/8/18.
//  Copyright © 2018 hassan. All rights reserved.
//

import UIKit

class NewOrderViewController: UIViewController {

    
    @IBOutlet weak var viDate: UIView!
    @IBOutlet weak var lblDateValue: UILabel!
    @IBOutlet weak var lblTimeValue: UILabel!
    @IBOutlet weak var lblTotalPriceValue: UILabel!
    @IBOutlet weak var lblNumberOfHour: UILabel!
    @IBOutlet weak var viTime: UIView!
    @IBOutlet weak var viNumbOfHour: UIView!
    @IBOutlet weak var btnLocation: UIButton!
    @IBOutlet weak var viAlert: UIView!
    @IBOutlet weak var lblAlertMsg: UILabel!
    
    var selectedDate : String?
    var selectedTime : String?
    var selectedNumberOfHour : String?
    var totalPrice : String?
    var lat , lng , address : String?
    
    
    var orderViewModel = OrderViewModel()
    var teacherData : SearchResultModel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let backBut = UIBarButtonItem.init(image: UIImage.init(named: "leftArrow")!, style: .plain, target: self, action: #selector(backButAct))
        self.navigationItem.leftBarButtonItem = backBut
        title = "طلب جديد"
        //===========================
        viDate.layer.borderColor = #colorLiteral(red: 0.1137254902, green: 0.7490196078, blue: 0.8235294118, alpha: 1).cgColor
        viTime.layer.borderColor = #colorLiteral(red: 0.1137254902, green: 0.7490196078, blue: 0.8235294118, alpha: 1).cgColor
        viNumbOfHour.layer.borderColor = #colorLiteral(red: 0.1137254902, green: 0.7490196078, blue: 0.8235294118, alpha: 1).cgColor
        btnLocation.layer.borderColor = #colorLiteral(red: 0.1137254902, green: 0.7490196078, blue: 0.8235294118, alpha: 1).cgColor
        //============================
        
        //============================================
//        orderViewModel.failureHappened = { (message) in
//            self.showErrorWithMessage(message: message)
//        }
//        orderViewModel.orderSuccessedDone = { (message) in
//            self.showAlertWithMessageAndTitle(title: "تم", message: message)
//            
//        }
        //============================================
        
        
    }

    @objc func backButAct() ->Void {
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func btnDatePickerAct(_ sender: Any) {
        DatePickerDialog().show("التاريخ", doneButtonTitle: "تم", cancelButtonTitle: "الغاء", defaultDate: Date(), minimumDate: Date(), maximumDate: nil, datePickerMode: .date) { (date) in
            if let selectDate = date {
                self.selectedDate = selectDate.toString(dateFormat: "yyyy-MM-dd")
                self.lblDateValue.text = selectDate.toString(dateFormat: "yyyy-MM-dd")
                
                
            }
        }
        
    }
    @IBAction func btnTimePickerAct(_ sender: Any) {
        DatePickerDialog().show("الوقت", doneButtonTitle: "تم", cancelButtonTitle: "الغاء", defaultDate: Date(), minimumDate: nil, maximumDate: nil, datePickerMode: .time) { (date) in
            if let selectDate = date {
                var actualHours :Int!
                var timePer : String
                let hours = Int(selectDate.toString(dateFormat: "HH"))
                let minutes = selectDate.toString(dateFormat: "mm")
                if hours! > 12 {
                    actualHours = hours! - 12
                    timePer = "ليلآ"
                }else{
                    actualHours = hours
                    timePer = "نهارآ"
                }
                self.lblTimeValue.text = "\(actualHours!):\(minutes)  \(timePer)"
                
                self.selectedTime = selectDate.toString(dateFormat: "HH:mm:ss")
            }
        }
    }
    @IBAction func btnNumOfHourAct(_ sender: Any) {
        let dataStr = (1...10).map { (val) -> String in
            return "\(val)"
        }
        print(dataStr)
        McPicker.show(data: [dataStr]) { [weak self] (selection : [Int : String]) in
            
            let selected = selection[0]
            self?.lblNumberOfHour.text = "\(selected!) ساعة"
            self?.selectedNumberOfHour = selected
            let totalprice = Double(selected!)! * Double((self?.teacherData.teacherSubjects?.price)!)
            self?.lblTotalPriceValue.text = "\(totalprice) ريال/\(selected!) ساعة"
            
            self?.totalPrice = "\(totalprice)"
            
        }
    }
    
    @IBAction func btnLocationAct(_ sender: Any) {
        let vc = getViewControllerFromStroyBoard(StoryBoard: "Main", bund: nil, identifierVC: "SelectLocationViewController") as! SelectLocationViewController
        
        vc.delegate = self
        let nav = UINavigationController.init(rootViewController: vc)
        present(nav, animated: true, completion: nil)
        
    }
    @IBAction func dismissRedAlertAct(_ sender: Any) {
        viAlert.isHidden = true
    }
    @IBAction func btnOrderAct(_ sender: Any) {
        
        if selectedTime == nil {
            viAlert.isHidden = false
            return
        }
        if selectedDate == nil {
            viAlert.isHidden = false
            return
        }
        if selectedNumberOfHour == nil {
            viAlert.isHidden = false
            return
        }
        
        if lat == nil {
            viAlert.isHidden = false
            return
        }
        viAlert.isHidden = true
        
        
        print("\(teacherData.id) >> \(selectedDate) , \(selectedTime) >> \(selectedNumberOfHour) , \(lat) , \(lng) , \(address) , \(totalPrice) ,, \((teacherData.teacherSubjects?.id)!)")
        
        orderViewModel.setParameterMakeOrder(teacherID: "\(teacherData.id!)", orderDate: selectedDate!, orderTime: selectedTime!, seassionHours: selectedNumberOfHour!, lat: lat!, lng: lng!, address: address!, subjectId: "\((teacherData.teacherSubjects?.subjectId)!)")
        let vc = getViewControllerFromStroyBoard(StoryBoard: "Main", bund: nil, identifierVC: "OrderDetailsViewController") as! OrderDetailsViewController
        vc.orderViewModel = orderViewModel
        vc.numberOfHour = selectedNumberOfHour
        vc.totalPrice = totalPrice
        vc.name = teacherData.name!
        vc.time = selectedTime!
        vc.date = selectedDate!
        self.navigationController?.show(vc, sender: nil)
//        orderViewModel.makeOrder()
        
    }
    
}





extension NewOrderViewController : SelectLocationDelegate
{
    func locationAndAddressData(lat: String, lng: String, address: String) {
        self.lat = lat
        self.lng = lng
        self.address = address
    }
    
    
    
    
}
