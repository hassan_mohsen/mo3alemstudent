//
//  FavouritViewController.swift
//  mo3alemStudent
//
//  Created by hassan on 6/25/18.
//  Copyright © 2018 hassan. All rights reserved.
//

import UIKit

class FavouritViewController: UIViewController {

    
    @IBOutlet weak var tblFavourit: UITableView!
    
    var tableData = [FavouriteModel]() {
        didSet{
            tblFavourit.reloadData()
        }
    }
    var favouritViewModel = FavouritViewModel()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.navigationBar.tintColor = .white
        tblFavourit.register(UINib.init(nibName: "FavouritCustomCell", bundle: nil), forCellReuseIdentifier: "cell")
    
        //======================================
        favouritViewModel.responseClouser = {[weak self] (message) in
            
            self?.showErrorWithMessage(message: message)
        }
        
        favouritViewModel.listOfFavourites = { [weak self] (favourites) in
            self?.tableData = favourites
        }
        //======================================
        favouritViewModel.getMyFavourites()
        
        
        
        
    }

    
}



extension FavouritViewController : UITableViewDataSource
{
    func numberOfSections(in tableView: UITableView) -> Int {
        return tableData.count
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! FavouritCustomCell
        
        let obj = tableData[indexPath.section]
        
        cell.cellLblName.text = obj.name
        cell.cellImgProfile.image = obj.gender == 0 ? #imageLiteral(resourceName: "MALE") : #imageLiteral(resourceName: "FEMALE")
        cell.cellImgProfile.moa.url = obj.image
        
        cell.cellBtnFav.tag = indexPath.section
        cell.cellBtnFav.addTarget(self, action: #selector(changeFavState(sender:)), for: .touchUpInside)
        
        //======================================
        cell.layer.cornerRadius = 5
        cell.layer.masksToBounds = true
        cell.backgroundColor = #colorLiteral(red: 0.4941176471, green: 0.5411764706, blue: 0.6274509804, alpha: 1)
        //======================================
        return cell
    }
    
    @objc func changeFavState(sender : UIButton) -> Void {
        let obj = tableData[sender.tag]
        
        let favViewModel = FavouritViewModel()
        favViewModel.responseClouser = {[weak self] (message) in
            self?.tableData.remove(at: sender.tag)
            self?.showErrorWithMessage(message: message)
            self?.tblFavourit.reloadData()
        }
        
        favViewModel.setData(teacherID: "\(obj.id!)")
        favViewModel.changFavouritStat()
        
        
    }
    
    
}



extension FavouritViewController : UITableViewDelegate
{
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 5
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 80
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        
        
        let vc = getViewControllerFromStroyBoard(StoryBoard: "Main", bund: nil, identifierVC: "TeacherProfileViewController") as! TeacherProfileViewController
        
        
        let teacherViewModel = TeacherDataViewModel()
        teacherViewModel.errorResponse = { [weak self] (message) in
            self?.showErrorWithMessage(message: message)
        }
        teacherViewModel.successResponse = { [weak self] (model) in
//            self?.navigationController.pu
            vc.teacherData = model
//            self?.navigationController?.pushViewController(vc, animated: true)
                self?.present(vc, animated: true, completion: nil)
        }
        
        
        teacherViewModel.setData(setID: "\(tableData[indexPath.section].id!)")
        teacherViewModel.getTeacherData()
        
    }
}



