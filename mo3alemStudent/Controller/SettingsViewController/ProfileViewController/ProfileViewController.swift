//
//  ProfileViewController.swift
//  mo3alemStudent
//
//  Created by hassan on 6/26/18.
//  Copyright © 2018 hassan. All rights reserved.
//

import UIKit

class ProfileViewController: UIViewController {

    
    @IBOutlet weak var tblProfile: UITableView!
    
    var tableData = [EditeProfileModel]()
    
    @IBOutlet weak var imgProfile: UIImageView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tblProfile.register(UINib.init(nibName: "ProfileCustomCell", bundle: nil), forCellReuseIdentifier: "cell") 
//        self.navigationController?.navigationBar.barTintColor = #colorLiteral(red: 0.1137254902, green: 0.7450980392, blue: 0.8156862745, alpha: 1)
        
        
        tblProfile.tableFooterView = footerTableView()
        let userModel = UserDataActions.getUserModel()
        
        imgProfile.image = userModel?.gender == 0 ? #imageLiteral(resourceName: "MALE") : #imageLiteral(resourceName: "FEMALE")
        
        imgProfile.moa.url = userModel?.image
        
        
        
        tableData.append(EditeProfileModel(title: "الإسم" , value: userModel?.name ?? "", canEditable: false) )
        tableData.append(EditeProfileModel(title: "البريد الإلكتروني", value: userModel?.email ?? "", canEditable: false))
        tableData.append(
            EditeProfileModel.init(title: "رقم الهاتف", value: userModel?.phone ?? "", canEditable: false))
        
        tblProfile.reloadData()
        
        
        let rightBut = UIBarButtonItem.init(image: #imageLiteral(resourceName: "EditeProfile"), style: .plain, target: self, action: #selector(rightButAct))
        self.navigationItem.rightBarButtonItem = rightBut
        
        
        
    }

    
   
    
    
    @objc func rightButAct() -> Void {
        
        
        tableData = tableData.map({ (model) -> EditeProfileModel in
            var mod = model
            mod.canEditable = !mod.canEditable
            return mod
        })
        tblProfile.reloadData()

        
    }
    
}



extension ProfileViewController : UITableViewDataSource
{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return tableData.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! ProfileCustomCell
        
        let obj = tableData[indexPath.row]
        cell.cellLblTitle.text = obj.title
        cell.cellTxtData.text = obj.value
        cell.cellTxtData.textAlignment = .right
        cell.cellTxtData.isEnabled = obj.canEditable

        
        
        return cell
    }
    
    
}





//MARK:- helper methods
extension ProfileViewController
{
    func footerTableView() -> UIView {
        let vi = UIView.init(frame: CGRect(x: 0, y: 0, width: tblProfile.bounds.width, height: 70))
        vi.backgroundColor = .white
        
        let but = UIButton.init(frame: CGRect(x: 25, y: 28, width: tblProfile.bounds.width - 50, height: 35))
        but.backgroundColor = #colorLiteral(red: 0.6588235294, green: 0.6588235294, blue: 0.6588235294, alpha: 1)
        but.setTitle("ملاحظاتك", for: .normal)
        but.setTitleColor(.white, for: .normal)
        but.setImage(UIImage.init(named: "ProfileNote")!, for: .normal)
        but.semanticContentAttribute = .forceRightToLeft
        but.layer.cornerRadius = 5.0
        
        
        but.addTarget(self, action: #selector(myNotesButAct) , for: .touchUpInside)
        
        
        vi.addSubview(but)
        return vi
    }
    
    
    @objc private func myNotesButAct() -> Void {
        
        
        
        
    }
}


