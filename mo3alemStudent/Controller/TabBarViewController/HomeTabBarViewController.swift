//
//  HomeTabBarViewController.swift
//  mo3alemStudent
//
//  Created by hassan on 9/6/18.
//  Copyright © 2018 hassan. All rights reserved.
//

import UIKit

class HomeTabBarViewController: UITabBarController {

    override func viewDidLoad() {
        super.viewDidLoad()
        
        let vcHome = getViewControllerFromStroyBoard(StoryBoard: "Main", bund: nil, identifierVC: "SearchViewController") as! SearchViewController
        vcHome.tabBarItem = UITabBarItem.init(title: nil, image: #imageLiteral(resourceName: "TabSearch").withRenderingMode(.alwaysOriginal), selectedImage: #imageLiteral(resourceName: "searchTool").withRenderingMode(.alwaysOriginal))
        self.viewControllers = [vcHome]
        
        
        let vcNotif = getViewControllerFromStroyBoard(StoryBoard: "Main", bund: nil, identifierVC: "NotificationsViewController") as! NotificationsViewController
        vcNotif.tabBarItem = UITabBarItem.init(title: nil, image: #imageLiteral(resourceName: "TabNotif").withRenderingMode(.alwaysOriginal), selectedImage: #imageLiteral(resourceName: "TabNotifSelected").withRenderingMode(.alwaysOriginal))
        let navNotif = UINavigationController.init(rootViewController: vcNotif)
        navNotif.navigationBar.barTintColor = #colorLiteral(red: 0.1137254902, green: 0.7450980392, blue: 0.8156862745, alpha: 1)
        self.viewControllers?.append(navNotif)
        
        let vcOrders = getViewControllerFromStroyBoard(StoryBoard: "Main", bund: nil, identifierVC: "MyOrderViewController") as! MyOrderViewController
        vcOrders.tabBarItem = UITabBarItem.init(title: nil, image: #imageLiteral(resourceName: "TabMyOrder").withRenderingMode(.alwaysOriginal), selectedImage: #imageLiteral(resourceName: "TabMyOrderSelected").withRenderingMode(.alwaysOriginal))
        let navOrders = UINavigationController.init(rootViewController: vcOrders)
        navOrders.navigationBar.barTintColor = #colorLiteral(red: 0.1137254902, green: 0.7450980392, blue: 0.8156862745, alpha: 1)
        self.viewControllers?.append(navOrders)
        
        let vcWallet = getViewControllerFromStroyBoard(StoryBoard: "Main", bund: nil, identifierVC: "MyWalletViewController") as! MyWalletViewController
        vcWallet.tabBarItem = UITabBarItem.init(title: nil, image: #imageLiteral(resourceName: "TabMyWallet").withRenderingMode(.alwaysOriginal), selectedImage: #imageLiteral(resourceName: "TabMyWalletSelected").withRenderingMode(.alwaysOriginal))
        let navWallet = UINavigationController.init(rootViewController: vcWallet)
        navWallet.navigationBar.barTintColor = #colorLiteral(red: 0.1137254902, green: 0.7450980392, blue: 0.8156862745, alpha: 1)
        self.viewControllers?.append(navWallet)
        
        
        let vcProfile = getViewControllerFromStroyBoard(StoryBoard: "Main", bund: nil, identifierVC: "ProfileViewController") as! ProfileViewController
        vcProfile.tabBarItem = UITabBarItem.init(title: nil, image: #imageLiteral(resourceName: "TabProfile").withRenderingMode(.alwaysOriginal), selectedImage: #imageLiteral(resourceName: "TabProfileSelected").withRenderingMode(.alwaysOriginal))
        let navProfile = UINavigationController.init(rootViewController: vcProfile)
        navProfile.navigationBar.barTintColor = #colorLiteral(red: 0.1137254902, green: 0.7450980392, blue: 0.8156862745, alpha: 1)
        navProfile.navigationBar.tintColor = .white
        self.viewControllers?.append(navProfile)
        
        
        
    }

    
    
    
    

}
