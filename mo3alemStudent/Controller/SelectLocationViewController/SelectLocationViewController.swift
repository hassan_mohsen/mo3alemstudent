//
//  SelectLocationViewController.swift
//  mo3alemStudent
//
//  Created by hassan on 7/8/18.
//  Copyright © 2018 hassan. All rights reserved.
//

import UIKit
import MapKit


protocol SelectLocationDelegate {
    func locationAndAddressData(lat : String , lng : String , address : String) -> Void
}

class SelectLocationViewController: UIViewController {

    
    
    
    
    
    @IBOutlet weak var lblAddress: UILabel!
    @IBOutlet weak var viMapKit: MKMapView!
    @IBOutlet weak var txtLocationName: UITextField!
    @IBOutlet weak var viHeaderLocationName: UIView!
    //=================================
    var currentLocation : CLLocationCoordinate2D?
    var currentAddress : String?
    var annotaion : MKPointAnnotation?
    //=================================
    
    
    var locationManager: CLLocationManager!
    var delegate : SelectLocationDelegate!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        //===================================
        txtLocationName.layer.borderWidth = 0.8
        txtLocationName.layer.borderColor = UIColor.lightGray.cgColor
        txtLocationName.layer.cornerRadius = 3.0
        txtLocationName.layer.masksToBounds = true
        //======================================
        if (CLLocationManager.locationServicesEnabled())
        {
            locationManager = CLLocationManager()
            locationManager.delegate = self
            locationManager.desiredAccuracy = kCLLocationAccuracyBest
            locationManager.requestAlwaysAuthorization()
            locationManager.startUpdatingLocation()
        }
        
        
        let gestureRecognizer = UITapGestureRecognizer(target: self, action:#selector(handleTap(gestureReconizer:)))
//        gestureRecognizer.delegate = self
        gestureRecognizer.numberOfTapsRequired = 1
        gestureRecognizer.numberOfTouchesRequired = 1
        viMapKit.addGestureRecognizer(gestureRecognizer)
        
        
        
    }

    @objc func handleTap(gestureReconizer: UITapGestureRecognizer) {
        
        let location = gestureReconizer.location(in: viMapKit)
        let coordinate = viMapKit.convert(location,toCoordinateFrom: viMapKit)
        
        
        let annotations = viMapKit.annotations
        viMapKit.removeAnnotations(annotations)
        
        
        let annotation = MKPointAnnotation()
        annotation.coordinate = coordinate
        viMapKit.addAnnotation(annotation)
        
        self.currentLocation = coordinate
        self.convertCoordinatesToAddress(lat: coordinate.latitude, long: coordinate.longitude)

        
        
    }
    
    @IBAction func butDoneAct(_ sender: Any) {
        
        if currentLocation == nil || currentAddress == nil {
            showErrorWithMessage(message: "من فضلك اختر الموقع الخاص بالحصة")
            return
        }
        
        self.delegate.locationAndAddressData(lat: "\(currentLocation!.latitude)", lng: "\(currentLocation!.longitude)", address: currentAddress!)
        
        self.navigationController?.dismiss(animated: true, completion: nil)
    }
    
    
    

}


extension SelectLocationViewController : CLLocationManagerDelegate
{
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        if let location = locations.last{
            let center = CLLocationCoordinate2D(latitude: location.coordinate.latitude, longitude: location.coordinate.longitude)
            let region = MKCoordinateRegion(center: center, span: MKCoordinateSpan(latitudeDelta: 0.01, longitudeDelta: 0.01))
            self.viMapKit.setRegion(region, animated: true)
            self.currentLocation = region.center
            self.convertCoordinatesToAddress(lat: center.latitude, long: center.longitude)
            
        }
    }
    
    
    func convertCoordinatesToAddress(lat :Double , long : Double) -> Void {
        
        let geoCoder = CLGeocoder()
        let location = CLLocation(latitude: lat, longitude: long)
        geoCoder.reverseGeocodeLocation(location, completionHandler: { (placemarks, error) -> Void in
            
            // Place details
            var placeMark: CLPlacemark!
            placeMark = placemarks?[0]
            
            self.lblAddress.text = placeMark.makeAddressString()
            self.currentAddress = placeMark.makeAddressString()
            let london = MKPointAnnotation()
            london.title = "\(placeMark.makeAddressString())"
            london.coordinate = CLLocationCoordinate2D.init(latitude: lat, longitude: long)
            self.viMapKit.addAnnotation(london)
            self.annotaion = london
        })
        
        
    }
    
}

extension SelectLocationViewController : MKMapViewDelegate
{
    
}
