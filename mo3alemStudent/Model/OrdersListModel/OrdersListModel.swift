//
//  OrdersListModel.swift
//  mo3alemStudent
//
//  Created by hassan on 9/8/18.
//  Copyright © 2018 hassan. All rights reserved.
//

import Foundation
import AVFoundation


struct OrderListModel : Codable {
//    var today : [OrderModel]?
//    var before : [OrderModel]?
    
    
    
    var today : [OrderNewModel]?
    var before : [OrderNewModel]?
    
}


struct OrderNewModel: Codable {
    let id: Int?
    let orderDate, orderTime, sessionHours, lat: String?
    let lng, address: String?
    let priceSubject, couponID, priceDiscount, status: Int?
    let createdAt, updatedAt: String?
    let student, teacher: Student?
    let subject: SubjectModel?
    
    var cellHeight : CGFloat = ExpandTableHeight.collaps.rawValue
    
    enum CodingKeys: String, CodingKey {
        case id
        case orderDate = "order_date"
        case orderTime = "order_time"
        case sessionHours = "session_hours"
        case lat, lng, address
        case priceSubject = "price_subject"
        case couponID = "coupon_id"
        case priceDiscount = "price_discount"
        case status
        case createdAt = "created_at"
        case updatedAt = "updated_at"
        case student, teacher, subject
    }
}

struct Student: Codable {
    let id: Int?
    let name, email, phone: String?
    let isTeacher, gender, isAvailable, showPhone: Int?
    
    enum CodingKeys: String, CodingKey {
        case id, name, email, phone
        case isTeacher = "is_teacher"
        case gender
        case isAvailable = "is_available"
        case showPhone = "show_phone"
    }
}

enum ExpandTableHeight : CGFloat {
    case Expand = 200.0
    case collaps = 120.0
}


/*
struct Subject: Codable {
    let id: Int?
    let name: String?
    let minPrice, maxPrice, stageID, status: Int?
    
    enum CodingKeys: String, CodingKey {
        case id, name
        case minPrice = "min_price"
        case maxPrice = "max_price"
        case stageID = "stage_id"
        case status
    }
}


struct OrderModel : Codable {
    
    let id : Int?
    let status : Int?
    let teacherName : String?
    let created : String?
    let updated : String?
    
}

*/



