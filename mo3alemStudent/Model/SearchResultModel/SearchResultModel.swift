//
//  SearchResultModel.swift
//  mo3alemStudent
//
//  Created by hassan on 9/3/18.
//  Copyright © 2018 hassan. All rights reserved.
//

import Foundation



struct SearchResultModel : Codable {
    
    let id : Int?
    let name : String?
    let email : String?
    let phone : String?
    let image : String?
    let isTeacher : Int?
    let gender : Int?
    let isAvailable : Int?
    let showPhone : Int?
    let apiToken : String?
    let isSuspend : Int?
    let message : String?
    let avgRate : Int?
    let nationality : String?
    let experience : String?
    let teacherCertificate : TeacherCertificate?
    let teacherSubjects : TeacherSubject?
    
    enum CodingKeys : String, CodingKey {
        case id
        case name , email, phone , image
        case isTeacher = "is_teacher"
        case isAvailable = "is_available"
        case showPhone = "show_phone"
        case apiToken = "api_token"
        case isSuspend = "is_suspend"
        case gender
        case message
        case avgRate
        case nationality
        case experience
        case teacherCertificate = "certificate"
        case teacherSubjects = "subject"
    }
    
    
}

struct TeacherSubject : Codable {
    let id : Int?
    let stageName : String?
    let subjectName : String?
    let price : Double?
    let subjectId : Int?
    enum CodingKeys : String, CodingKey {
        
        case subjectId = "subject_id"
        case id , stageName , subjectName , price
    }
    
}

struct TeacherCertificate: Codable {
    let userId : Int?
    let universityCertificate : String?
    let experienceCertificate : String?
    let otherCertificate : String?
    
    enum CodingKeys : String , CodingKey {
        case userId = "user_id"
        case universityCertificate = "university_certificate"
        case experienceCertificate = "experience_certificate"
        case otherCertificate = "other_certificate"
    }
}


