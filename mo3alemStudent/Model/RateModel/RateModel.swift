//
//  RateModel.swift
//  Mo3alemTeacher
//
//  Created by hassan on 8/29/18.
//  Copyright © 2018 hassan. All rights reserved.
//

import Foundation


struct RateModel : Codable {
    
    let studentId , teacherId : Int?
    let langRate  , explainRate ,practicalRate ,timeRate: Float?
    let total : Float?
    let notes : String
    
    
    
    enum CodingKeys: String, CodingKey {
        case studentId = "student_id"
        case teacherId  = "teacher_id"
        case explainRate = "explain_rate"
        case practicalRate = "practical_rate"
        case timeRate = "time_rate"
        case langRate = "lang_rate"
        case total
        case notes
        
        
    }
}

