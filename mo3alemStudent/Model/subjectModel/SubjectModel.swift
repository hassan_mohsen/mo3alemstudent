//
//  SubjectModel.swift
//  Mo3alemTeacher
//
//  Created by hassan on 8/29/18.
//  Copyright © 2018 hassan. All rights reserved.
//

import Foundation


struct SubjectModel : Codable {
    
    let name : String?
    let minprice : Int?
    let maxprice : Int?
    let stageid : Int? // was String
    let status : Int? // was String
    let id : Int?
    
    enum CodingKeys: String, CodingKey {
        case minprice = "min_price"
        case stageid  = "stage_id"
        case maxprice = "max_price"
        case name , status
        case id
    }
    
}
