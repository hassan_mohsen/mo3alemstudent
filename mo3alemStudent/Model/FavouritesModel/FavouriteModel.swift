//
//  FavouriteModel.swift
//  mo3alemStudent
//
//  Created by hassan on 9/5/18.
//  Copyright © 2018 hassan. All rights reserved.
//

import Foundation

struct FavouriteModel : Codable {
    
    let id : Int?
    let name : String?
    let gender : Int?
    let image : String?
    
    
}


