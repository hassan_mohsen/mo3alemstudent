//
//  TeacherDataModel.swift
//  mo3alemStudent
//
//  Created by hassan on 9/9/18.
//  Copyright © 2018 hassan. All rights reserved.
//

import Foundation


//struct FavTeacherData : Codable {
//    
//    let ratesCount : RatesCount?
//    let data : TeacherDataModel?
//    
//}

struct FavTeacherData : Codable {
    
    let id : Int?
    let name : String?
    let email : String?
    let phone : String?
    let image : String?
    let isTeacher : Int?
    let gender : Int?
    let isAvailable : Int?
    let showPhone : Int?
    let apiToken : String?
    let isSuspend : Int?
    let message : String?
    let avgRate : Int?
    let nationality : String?
    let experience : String?
    let ratesCount : RatesCount?
    let teacherCertificate : TeacherCertificate?
    let teacherSubjects : [TeacherSubject]?
    
    enum CodingKeys : String, CodingKey {
        case ratesCount
        case id
        case name , email, phone , image
        case isTeacher = "is_teacher"
        case isAvailable = "is_available"
        case showPhone = "show_phone"
        case apiToken = "api_token"
        case isSuspend = "is_suspend"
        case gender
        case message
        case avgRate
        case nationality
        case experience
        case teacherCertificate = "certificate"
        case teacherSubjects = "subjects"
    }
    
    
}
    
struct RatesCount : Codable {
    let langRate : Int?
    let explainRate : Int?
    let practicalRate : Int?
    let timeRate : Int?
    
    enum CodingKeys : String, CodingKey {
        case langRate = "lang_rate"
        case explainRate = "explain_rate"
        case practicalRate = "practical_rate"
        case timeRate = "time_rate"
    }
    
    
}
