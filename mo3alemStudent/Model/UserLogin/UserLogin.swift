//
//  UserLogin.swift
//  Mo3alemTeacher
//
//  Created by hassan on 8/29/18.
//  Copyright © 2018 hassan. All rights reserved.
//

import Foundation


struct UserLogin : Codable {
    let id : Int?
    let name : String?
    let email : String?
    let phone : String?
    let image : String?
    let isTeacher : Int?
    let gender : Int?
    let isAvailable : Int?
    let showPhone : Int?
    let apiToken : String?
    let isSuspend : Int?
    let message : String?
    enum CodingKeys : String, CodingKey {
        case id
        case name , email, phone , image
        case isTeacher = "is_teacher"
        case isAvailable = "is_available"
        case showPhone = "show_phone"
        case apiToken = "api_token"
        case isSuspend = "is_suspend"
        case gender
        case message
    }
}



struct UserDataActions {
    
    static func cashUserModel(user: UserLogin) ->Void {
        let userDictionary = try! user.asDictionary()
        
        UserDefaults.standard.set(userDictionary, forKey: "userData")
    
    }
    
    
    static func getApiToken() -> String {
        
        
        guard let userModel = UserDataActions.getUserModel() else {
            return ""
        }
        
        return userModel.apiToken!
    }

    
    static func getUserModel() -> UserLogin?
    {
        let cashedData = UserDefaults.standard.object(forKey: "userData") as! [String : Any]
        let data = try! JSONSerialization.data(withJSONObject: cashedData, options: .prettyPrinted)
        
        let decoder = JSONDecoder()
        do {
            let user = try decoder.decode(UserLogin.self, from: data)
            return user
        } catch {
            return nil
            print(error.localizedDescription)
        }

    }
 
    
}






