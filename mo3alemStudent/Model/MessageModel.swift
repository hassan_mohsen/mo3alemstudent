//
//  MessageModel.swift
//  mo3alemStudent
//
//  Created by hassan on 6/21/18.
//  Copyright © 2018 hassan. All rights reserved.
//

import Foundation
import UIKit


struct MessagesModel {
    
    let message : String
    let image : UIImage
    let btnColor : UIColor
    let ReSendHidden : Bool
    
    init(msg : String , img : String , color : UIColor , btnHidden : Bool) {
        
        self.message = msg
        self.image = UIImage.init(named: img)!
        self.btnColor = color
        self.ReSendHidden = btnHidden
        
    }
    
    
    
}


enum MessageType {
    case success(msg : MessageString , hiddenBut : Bool )
    case failur (msg : MessageString , hiddenBut : Bool )
}

enum MessageString : String {
    case Reg_success   = "dsfsdf"
    case Reg_failure   = "ss"
    case Order_success = "تم استقبال طلبك بنجاح"
    case Order_failure = "تعذر تنفيذ الطلب يرجي التآكد من المعلومات المدخله"
}
