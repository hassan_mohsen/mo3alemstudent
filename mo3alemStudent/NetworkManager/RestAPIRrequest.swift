//
//  RestAPIRrequest.swift
//  Aoun
//
//  Created by hassan on 5/6/18.
//  Copyright © 2018 hassan. All rights reserved.
//

import Foundation
import Alamofire



struct RestAPIRequest {
    
    
    static func SendRequest<T: Codable>(mod : T.Type , urlPath : URL , method : HTTPMethod = .get , parameter : [String : AnyObject]?, completion: @escaping (ServerResponse<T>) -> Void) -> Void {
        
        print("Methoddddd >>\(method.rawValue)")
        print("url >>>>>>>>. \(urlPath.description)")
        
        ARSLineProgress.show()

        
        request(urlPath.description, method: method , parameters: parameter, encoding: JSONEncoding.default).validate().responseData { (response) in
            ARSLineProgress.hide()
            print("response>>>>>")
            
            
            
            guard let data = response.result.value else {
                completion(ServerResponse<T>.failure(response.error))
                return
            }
            print("data come nowww>>> ")
            do {
                let decoder = JSONDecoder()
                let modules = try decoder.decode(mod, from: data)
                
                completion(ServerResponse<T>.success(modules))
            }catch {
                print("catch >>>>")
                completion(ServerResponse<T>.failure(error))
            }
            
        }
        
    }
    
}







// Enum For response >>>>>
enum ServerResponse<T> {
    case success(T), failure(Error?)
}


