//
//  RestAPIURLS.swift
//  Aoun
//
//  Created by hassan on 5/6/18.
//  Copyright © 2018 hassan. All rights reserved.
//

import Foundation

let BASE_URL = "http://jadeerapp.com/api/"

public enum APIRoutes : String {
    case login = "login"
    case subject = "subjects"
    case forgetPassword = "forgetPassword"
    case cities = "cities"
    case checkCode = "checkCode"
    case resetPassword = "resetPassword"
    case register = "registerStudent"
    case region   = "city/children"
    case stages = "stages"
    case search = "teacherData"
    case makeOrder = "makeOrder"
    case makeFavourite = "makeFavourite"
    case listFavourite = "listFavourite"
    case teacherDetails = "teacherDetails"
    case listStudentOrders = "listStudentOrders"
    case resendCode = "resendCode"
    case listOrders = "listOrders"
    
    
    
}



//Warning:- Minus Token Variable



enum RoutesFactory {
    
    case URLwithTokenParameter(route : APIRoutes)
    case URLClear(route:APIRoutes)
    case URLWithOneParameter(route : APIRoutes , key : String , value : String)
    case URLSearch(disId : String , subId : String , gen : String)
    var link : URL {
        switch self {
        case .URLwithTokenParameter(let route):
            return URL.init(string: BASE_URL + route.rawValue + "?apiToken=" + UserDataActions.getApiToken())!
        case .URLClear(let route):
            return URL.init(string: BASE_URL + route.rawValue)!
        case .URLWithOneParameter(let route, let key, let value) :
            return URL.init(string: BASE_URL + route.rawValue + "?" + key + "=" + value)!
        case .URLSearch(let disId , let subId , let gen) :
            return URL.init(string: BASE_URL + APIRoutes.search.rawValue + "?" + "dis=" + disId + "&sub=" + subId + "&gen=" + gen)!
//        default:
//            return URL.init(string: BASE_URL)!
        }
        
    }
}






