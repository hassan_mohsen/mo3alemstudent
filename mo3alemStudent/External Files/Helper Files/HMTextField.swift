//
//  HMTextField.swift
//  mo3alemStudent
//
//  Created by hassan on 6/6/18.
//  Copyright © 2018 hassan. All rights reserved.
//

import UIKit


class HMTextField: ACFloatingTextfield {

    private var __maxLengths = [UITextField: Int]()
    private var __SpecialValue : Bool?
    
    @IBInspectable var withSpecialCharacter : Bool {
        get {
            guard let val = __SpecialValue else{
                return false
            }
            return val
        }
        
        set {
            __SpecialValue = newValue
            addTarget(self, action: #selector(specialCharacter(textField:)), for: .editingChanged)
        }
    }
    @IBInspectable var MaxNumCharacter : Int {
        get {
            guard let l = __maxLengths[self] else {
                return 75 // (global default-limit. or just, Int.max)
            }
            return l
        }
        set {
            __maxLengths[self] = newValue
            addTarget(self, action: #selector(fix), for: .editingChanged)
        }
    }
    
    
    
    
    
    
    @objc func fix(textField: UITextField) {
        let t = textField.text
        textField.text = t?.safelyLimitedTo(length: MaxNumCharacter)
    }
    @objc func specialCharacter(textField : UITextField) -> Void {
        let t = textField.textNoNil() //"-/\\^`][ ?>=<;:0" >>> original
        let characterset = CharacterSet(charactersIn: "-/\\^`][ ?>=<;:")
        let filtered = t.components(separatedBy: characterset).joined(separator: "")
        textField.text = filtered
    }
}







extension String
{
    func safelyLimitedTo(length n: Int)->String {
        if (self.count <= n) {
            return self
        }
        return String( Array(self).prefix(upTo: n) )
    }
}
