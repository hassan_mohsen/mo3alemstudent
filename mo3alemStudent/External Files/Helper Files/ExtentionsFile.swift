//
//  ExtentionsFile.swift
//  ourCollectProject
//
//  Created by hassan on 6/23/17.
//  Copyright © 2017 hassan. All rights reserved.
//

import Foundation
import UIKit
import MapKit
//*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
// MARK: - extension For Images
extension UIImage {
    func convertToStringBase64(compressionQuality : CGFloat) -> String {
        
        let imageData:NSData = UIImageJPEGRepresentation(self, compressionQuality)! as NSData
        return imageData.base64EncodedString(options: .endLineWithLineFeed)
        
    }
    
    
    func resizeImage(newSize : CGSize) -> UIImage {
        
        UIGraphicsBeginImageContext(CGSize(width: newSize.width, height: newSize.height))
        self.draw(in: CGRect(x: 0, y: 0, width: newSize.width, height: newSize.height))
        var newImage = UIGraphicsGetImageFromCurrentImageContext()
        newImage = newImage?.withRenderingMode(.alwaysOriginal)
        UIGraphicsEndImageContext()
        
        return newImage!
        
        
    }
    
}

//*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
// MARK: - extension For String
extension String {

    //"yyyy-MM-dd'T'HH:mm:ss"
    func getDate(formateStr : String) -> Date? {
        

        var dateFormatter = DateFormatter()
        // Our date format needs to match our input string format
        dateFormatter.dateFormat = formateStr
        dateFormatter.timeZone = NSTimeZone(abbreviation: "GMT+0:00") as TimeZone!
        var dateFromString = dateFormatter.date(from: self)
        print("date >>>> \(dateFromString)")
        return dateFromString
        

        
        
//        let dateFormatter = DateFormatter()
//        dateFormatter.dateFormat = formateStr //Your date format
//        dateFormatter.timeZone = TimeZone(abbreviation: "GMT+0:00") //Current time zone
//        let date = dateFormatter.date(from: self) //according to date format your date string
//        print("convertDate \(date)")
//        return date
        
        
//        let dateFormatter = DateFormatter()
//        dateFormatter.dateFormat = formateStr
//        dateFormatter.timeZone = TimeZone.current
//        dateFormatter.locale = Locale.current
//        return dateFormatter.date(from: self) // replace Date String
    }
    
    
    func isValidEmail() -> Bool {
        // print("validate calendar: \(testStr)")
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,}"
        
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailTest.evaluate(with: self)//*-*-*-*-*-*-*-*-*-*-*-*-*-
    }
    
    
}
//*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
// MARK: - extention For Bundle
extension Bundle{
    
   /* func getSpecificBundleForLanguage() -> Bundle {
        let path = Bundle.main.path(forResource: cashedLanguage as! String?, ofType: "lproj")
        let bund = Bundle.init(path: path!)
        return bund!
    }*/
    
}
//*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
// MARK: - extension For Color
extension UIColor{
    
    
    
    convenience init(r: UInt8, g: UInt8 , b: UInt8 , a: CGFloat) {
        self.init(red: CGFloat(r)/255, green: CGFloat(g)/255, blue: CGFloat(b)/255, alpha: a)
    }
    convenience init(r: UInt8, g: UInt8 , b: UInt8 ) {
        self.init(red: CGFloat(r)/255, green: CGFloat(g)/255, blue: CGFloat(b)/255, alpha: 1.0)
    }
    
    public convenience init?(hexString: String) {
        let r, g, b, a: CGFloat
        //write without #
        //if hexString.hasPrefix("#") {
        //let start = hexString.index(hexString.startIndex, offsetBy: 1)
        //let hexColor = hexString.substring(from: start)
        let hexColor = hexString
        
        
        if hexColor.characters.count == 8 {
            let scanner = Scanner(string: hexColor)
            var hexNumber: UInt64 = 0
            
            if scanner.scanHexInt64(&hexNumber) {
                r = CGFloat((hexNumber & 0xff000000) >> 24) / 255
                g = CGFloat((hexNumber & 0x00ff0000) >> 16) / 255
                b = CGFloat((hexNumber & 0x0000ff00) >> 8) / 255
                a = CGFloat(hexNumber & 0x000000ff) / 255
                
                self.init(red: r, green: g, blue: b, alpha: a)
                return
            }
        }
        //}
        
        return nil
    }
}

//*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
// MARK: - extension uitext field
extension UITextField{
    
    func setButtonBorderColor(botoncolor : UIColor) -> Void {
        let border = CALayer()
        let borderWidth = CGFloat(1.5)
        border.borderColor = botoncolor.cgColor
        border.frame = CGRect(origin: CGPoint(x: 0,y :self.frame.size.height - borderWidth), size: CGSize(width: self.frame.size.width, height: self.frame.size.height))
        border.borderWidth = borderWidth
        self.layer.addSublayer(border)
        self.layer.masksToBounds = true
    }
    func setBottomBorder(txtBackground : UIColor ,bottomColor : UIColor) {
        self.borderStyle = .none
        
        self.layer.backgroundColor = txtBackground.cgColor
        self.layer.masksToBounds = false
        self.layer.shadowColor = bottomColor.cgColor
        self.layer.shadowOffset = CGSize(width: 0.0, height: 1.0)
        self.layer.shadowOpacity = 1.0
        self.layer.shadowRadius = 0.0
    }
    func setPaddingPoints(_ amount:CGFloat){
        let paddingView = UIView(frame: CGRect(x: 0, y: 0, width: amount, height: self.frame.size.height))
        self.leftView = paddingView
        self.leftViewMode = .always
        self.rightView = paddingView
        self.rightViewMode = .always
    }
    func setRightPaddingPoints(_ amount:CGFloat) {
        let paddingView = UIView(frame: CGRect(x: 0, y: 0, width: amount, height: self.frame.size.height))
        self.rightView = paddingView
        self.rightViewMode = .always
    }
    func textNoNil() -> String {
        // /* OR */ return text ?? ""
        
        if text != nil {
            return text!
        }
        return ""
    }
    
}

//*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
// MARK: - extension ViewController field
extension UIViewController{
    
    
    
    
    func prepareHomeTabViewControllers() -> UITabBarController
    {
        let searchVC = getViewControllerFromStroyBoard(StoryBoard: "Main", bund: nil, identifierVC: "SearchViewController") as! SearchViewController
        searchVC.tabBarItem = UITabBarItem.init(title: nil, image: UIImage.init(named: "TabSearch")!, selectedImage: UIImage.init(named: "TabSearchSelected")!)
        let navSearch = UINavigationController.init(rootViewController: searchVC)
        
        
        let notificatioVC = getViewControllerFromStroyBoard(StoryBoard: "Main", bund: nil, identifierVC: "NotificationsViewController") as! NotificationsViewController
        notificatioVC.tabBarItem = UITabBarItem.init(title: nil, image: UIImage.init(named: "TabNotif")!, selectedImage: UIImage.init(named: "TabNotifSelected")!)
        let navNotif = UINavigationController.init(rootViewController: notificatioVC)
        
        
        let myOrderVC = getViewControllerFromStroyBoard(StoryBoard: "Main", bund: nil, identifierVC: "MyOrderViewController") as! MyOrderViewController
        myOrderVC.tabBarItem = UITabBarItem.init(title: nil, image: UIImage.init(named: "TabMyOrder")!, selectedImage: UIImage.init(named: "TabMyOrderSelected")!)
        let navMyOrder = UINavigationController.init(rootViewController: myOrderVC)
        
        
        let myWalletVC = getViewControllerFromStroyBoard(StoryBoard: "Main", bund: nil, identifierVC: "MyWalletViewController") as! MyWalletViewController
        myWalletVC.tabBarItem = UITabBarItem.init(title: nil, image: UIImage.init(named: "TabMyWallet")!, selectedImage: UIImage.init(named: "TabMyWalletSelected")!)
        let navMyWallet = UINavigationController.init(rootViewController: myWalletVC)
        
        
        let profileVC = getViewControllerFromStroyBoard(StoryBoard: "Main", bund: nil, identifierVC: "ProfileViewController") as! ProfileViewController
        profileVC.tabBarItem = UITabBarItem.init(title: nil, image: UIImage.init(named: "TabProfile")!, selectedImage: UIImage.init(named: "TabProfileSelected")!)
        let navProfile = UINavigationController.init(rootViewController: profileVC)
        
        
        let tabbar = UITabBarController.init()
        
        tabbar.viewControllers = [navSearch , navNotif,navMyOrder , navMyWallet , navProfile]
        
        return tabbar
        
        
    }
    
    
    
    
    
    
    func getViewControllerFromStroyBoard(StoryBoard name:String , bund : Bundle? , identifierVC : String ) -> UIViewController {
        let storyBoard = UIStoryboard(name: name, bundle: bund)
        return storyBoard.instantiateViewController(withIdentifier: identifierVC)
    }
    
    func showAlertWithMessageAndTitle(title : String , message : String) -> Void {
        
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let ButOk = UIAlertAction(title: "OK", style: .default) { (action) in
            alert.dismiss(animated: true, completion: nil)
        }
        alert.addAction(ButOk)
        self.present(alert, animated: true, completion: nil)
        
    }
    
    func showErrorWithMessage(message : String) -> Void {
        
        self.showAlertWithMessageAndTitle(title: "خطآ", message: message)
        
    }
    
    func showAlertWithTitleAndMessageWith2Handler(title : String , message : String , doneTitle : String,otherTitle : String, doneHandler : @escaping (UIAlertAction) -> Void , otherHandler : @escaping (UIAlertAction) -> Void) -> Void {
        
        
        var mainAlert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        
        let ButOk = UIAlertAction(title: doneTitle, style: .default, handler: doneHandler)
        
        mainAlert.addAction(ButOk)
        
        
        if otherTitle.characters.count > 0 {
            let ButOther = UIAlertAction(title: otherTitle, style: .default, handler: otherHandler)
            mainAlert.addAction(ButOther)
            
        }
        
        
        self.present(mainAlert, animated: true, completion: nil)
    }
    
    func showAlertWithTitleAndMessageWith1Handler(title : String , message : String , doneTitle : String, doneHandler : @escaping (UIAlertAction) -> Void ) -> Void {
        
        showAlertWithTitleAndMessageWith2Handler(title: title, message: message, doneTitle: doneTitle, otherTitle: "", doneHandler: doneHandler) { (other) in
        }
        
    }
    
    
    
}

//*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
// MARK: - extension View
extension UIView {
    func setGradientColor(colors : [CGColor]) {
        let layer : CAGradientLayer = CAGradientLayer()
        layer.frame.size = self.frame.size
        //layer.frame.origin = CGPointMake(0.0,0.0)
        //layer.cornerRadius = CGFloat(frame.width / 20)
        layer.colors = colors
        
        self.layer.insertSublayer(layer, at: 0)
    }
    func BottomColorShadow(txtBackground : UIColor , bottomColor : UIColor ){
        //self.borderStyle = .none
        
        self.layer.backgroundColor = txtBackground.cgColor
        self.layer.masksToBounds = false
        self.layer.shadowColor = bottomColor.cgColor
        self.layer.shadowOffset = CGSize(width: 0.0, height: 1.0)
        self.layer.shadowOpacity = 1.0
        self.layer.shadowRadius = 0.0
    }
    
    
}
//MARK:- Encodable
extension Encodable {
    func asDictionary() throws -> [String: Any] {
        let data = try JSONEncoder().encode(self)
        guard let dictionary = try JSONSerialization.jsonObject(with: data, options: .allowFragments) as? [String: Any] else {
            throw NSError()
        }
        return dictionary
    }
}

// MARK:- Date
extension Date
{
    func toString( dateFormat format  : String ) -> String
    {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = format
        return dateFormatter.string(from: self)
    }
    
}
extension CLPlacemark {
    
    func makeAddressString() -> String {
        return [subThoroughfare, thoroughfare, locality, administrativeArea, postalCode, country]
            .flatMap({ $0 })
            .joined(separator: " ")
    }
    
    var customAddress: String {
        get {
            return [[thoroughfare, subThoroughfare], [postalCode, locality]]
                .map { (subComponents) -> String in
                    // Combine subcomponents with spaces (e.g. 1030 + City),
                    subComponents.flatMap({ $0 }).joined(separator: " ")
                }
                .filter({ return !$0.isEmpty }) // e.g. no street available
                .joined(separator: ", ") // e.g. "MyStreet 1" + ", " + "1030 City"
        }
    }
    
    
    
    
}

