//
//  UsersListCustomCell.swift
//  mo3alemStudent
//
//  Created by hassan on 6/8/18.
//  Copyright © 2018 hassan. All rights reserved.
//

import UIKit
import Cosmos


class UsersListCustomCell: UITableViewCell {

    
    @IBOutlet weak var cellBtnOrder: UIButton!
    @IBOutlet weak var cellLblName: UILabel!
    @IBOutlet weak var cellBtnCV: UIButton!
    @IBOutlet weak var cellLblNation: UILabel!
    @IBOutlet weak var cellViRate: CosmosView!
    @IBOutlet weak var cellLblPriceForHour: UILabel!
    
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
