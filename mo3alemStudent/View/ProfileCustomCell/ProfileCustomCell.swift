//
//  ProfileCustomCell.swift
//  mo3alemStudent
//
//  Created by hassan on 6/26/18.
//  Copyright © 2018 hassan. All rights reserved.
//

import UIKit

class ProfileCustomCell: UITableViewCell {

    
    @IBOutlet weak var cellLblTitle: UILabel!
    @IBOutlet weak var cellTxtData: UITextField!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
