//
//  MyOrderCustomCell.swift
//  mo3alemStudent
//
//  Created by hassan on 6/25/18.
//  Copyright © 2018 hassan. All rights reserved.
//

import UIKit

class MyOrderCustomCell: UITableViewCell {

    
    @IBOutlet weak var cellLbltitle: UILabel!
    @IBOutlet weak var cellimgStatus: UIImageView!
    @IBOutlet weak var cellLblStatus: UILabel!
    @IBOutlet weak var cellLblDate: UILabel!
    @IBOutlet weak var cellLblTime: UILabel!
    
    @IBOutlet weak var cellDarkBack: UILabel!
    @IBOutlet weak var cellBack: UILabel!
    @IBOutlet weak var cellConstDate: NSLayoutConstraint!
    @IBOutlet weak var cellConstTime: NSLayoutConstraint!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    // 0 - معلق
    // 1 - موافق
    // -1 - مرفوض
    // -2 - ملغاه
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
