//
//  FavouritCustomCell.swift
//  mo3alemStudent
//
//  Created by hassan on 6/25/18.
//  Copyright © 2018 hassan. All rights reserved.
//

import UIKit

class FavouritCustomCell: UITableViewCell {

    
    @IBOutlet weak var cellLblName: UILabel!
    @IBOutlet weak var cellImgProfile: UIImageView!
    @IBOutlet weak var cellBtnFav: UIButton!
    
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
